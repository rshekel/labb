import numpy as np 
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def main():
    #def r_square():
    path = r"..\results_W2\LightPower\power_vs_dist.txt"
    data = np.loadtxt(path, skiprows=1)
    r = data[:, 0]
    y =  data[:, 1]
    plt.figure()
    errs = y*0.05  # Just sounds reasonable...
    plt.errorbar(r,y, errs, fmt="*")
    
    
    r_sqr = lambda r, r0, a, c: a*np.power((r-r0), -2) + c
    
    start_A = 1000
    start_r0 = 2
    start_c = 0
        
    popt, pcov = curve_fit(r_sqr, r, y, p0=(start_r0, start_A, start_c))#, bounds=((0, -np.pi), (np.inf, np.pi)))
    
    params_errors = np.sqrt(np.diag(pcov))
    print_params(["R0", "A", "c"], popt, params_errors)
    
    x = np.linspace(np.min(r)*0.9, np.max(r)*1.1, 200)
    expected = r_sqr(x, *popt)
    
    plt.plot(x, expected, "r--", label=r"fit to $ \frac{a}{\left(r-r_{0}\right)^{2}}+c $")
    plt.legend()
    plt.title("Power vs Distance")
    plt.xlabel("Distance [cm]")
    plt.ylabel("Power [mV]")
    plt.grid()
    #plt.xlim(0, 30)
    #plt.ylim(0,50)
    
    # show in loglog
    r0_param = popt[0]
    c_param = popt[2]
    # Normalize r
    norm_r = r-r0_param
    norm_x = x-r0_param
    norm_y = y-c_param
    norm_expected = expected-c_param
    
    plt.figure("loglog")
    ax = plt.errorbar(norm_r,norm_y, errs, fmt="*")
    plt.plot(norm_x, norm_expected, "r--")
    plt.title("Power vs Distance - loglog")
    plt.xlabel("Distance [cm]")
    plt.ylabel("Power [mV]")
    plt.grid()
    
    ax = plt.axes()
    ax.set_xscale("log")
    ax.set_yscale("log")
    
    residuals(r, y, r_sqr, *popt)
    
    return popt, params_errors


def residuals(real_x, real_y, fit_func, *fit_func_params):
    plt.figure("Residuals")
    plt.title("Residuals")
    difference = real_y - fit_func(real_x, *fit_func_params)
    plt.plot(real_x, difference,'or')
    plt.plot([min(real_x)*0.9, max(real_x)*1.1], [0,0], "--")
    plt.xlabel("Distance [cm]")
    plt.ylabel(r"$ \Delta mV $")
    plt.grid()


def print_params(names, values, errors, rss=False):
    for name, value, error in zip(names, values, errors):
        if not rss:
            print("{}={:+.7f}~{:.7f}".format(name, value, error))
        else:
            err_percent = (error / value) * 100
            print("{}={:+.4f}+-{:.1f}%".format(name, value, err_percent))

if __name__ == "__main__":
    plt.close("all")
    mpl.rcParams['font.size'] = 16
    main()
import numpy as np 
import matplotlib as mpl
import matplotlib.pyplot as plt
from fourier import *
from fit_rsqr import * 
from scipy.optimize import curve_fit

def fit_to_exp(path, ignore_up_factor=0.8, ignore_down_factor=0.01, should_plot=True):

    # %% start 
    # ignore_up_factor=0.95
    # ignore_down_factor=0.01
    #path = r"..\results_W3\8-blame_who-7.2cm-400mV-400KHZ.txt"
    xx, y = load_data(path)
    
    # Fix the time vector, due to low resolution in saved data
    dx = np.diff(xx)[0]
    x = np.arange(xx[0], xx[-1]+dx, dx)
    
    # Make x and y shorter' for better analyzing of the slope
    x = x[:500]
    y = y[:500]
    
    # %% remove unimportant first const values
    if should_plot:
        plt.figure(path)
        plt.plot(x,y, '.')

    some_high_index = np.where(y > y.max()*ignore_up_factor)[0][0]
    y1 = y[some_high_index:]
    x1 = x[some_high_index:]
    slope_start_index = np.where(y1 < y1.max()*ignore_up_factor)[0][0] # - 1 
    slope_end_index =   np.where(y1 < (y1.min()+np.abs(y1.min()*ignore_down_factor)))[0][0]  
    slope_end_index += 30 # I think that longer slope, with more "0" helps for the fit
    
    x2 = x1[slope_start_index:slope_end_index]
    y2 = y1[slope_start_index:slope_end_index]

    if should_plot:
        plt.plot(x2, y2, '.')
        # plt.xlim(x1[slope_start_index-10], x1[slope_end_index+10])
        
    # %% Fit to exp
    exp = lambda y, a, b, c, x0: a*np.exp(-b*(y-x0)) + c
    
    start_a = 5e9
    start_b = 1e5
    start_c = -0.5
    start_x0 = x2[0]
    
    # popt, pcov = curve_fit(exp, x2, y2, p0=(start_a, start_b, start_c), maxfev=10000)
    popt, pcov = curve_fit(exp, x2, y2, p0=(start_a, start_b, start_c, start_x0), bounds=((1e6, 1e4, -10, 0), (1e13, 1e8, 10, 0.001)))
    
    params_errors = np.sqrt(np.diag(pcov))
    print_params(["a", "b", "c", "x0"], popt, params_errors, rss=True)

    x = np.linspace(np.min(x2)*0.98, np.max(x2)*1.1, 200)
    expected = exp(x, *popt)
    
    if should_plot:
        plt.plot(x, expected, "r--", label=r"fit to $ a\cdot e^{-bt}+c $")
        plt.legend()
        plt.title("fit to exp")
        plt.ylabel("amp. [V]")
        plt.xlabel("time [s]")
        plt.grid()
    
    step_size = np.max(y) - np.min(y) 

    # %%    
    return popt, params_errors, step_size
    

def analyze(paths, by_step_size, should_plot=True):
    popts = []
    params_errorss = []
    step_sizes = []
    for path in paths:
        popt, params_errors, step_size = fit_to_exp(path, should_plot=should_plot)
        popts.append(popt)
        params_errorss.append(params_errors)
        step_sizes.append(step_size)
    
    
    bs = np.array([x[1] for x in popts])
    b_errs = np.array([x[1] for x in params_errorss])
    
    taus = 1/bs
    # by partial derivative...
    tau_errs = (1/bs**2)*b_errs
    
    plt.figure()
    #plt.ylabel("decay factor tau [s]")
    plt.ylabel(r"Lifetime $ \tau [\mu s] $")
    taus = taus * 1e6
    tau_errs *= 1e6
    if by_step_size:
        step_sizes = np.array(step_sizes)
        sorted_indexes = np.argsort(step_sizes)
        step_sizes = step_sizes[sorted_indexes]
        taus = taus[sorted_indexes]
        tau_errs = tau_errs[sorted_indexes]
        bs = bs[sorted_indexes]
        b_errs = b_errs[sorted_indexes]
        
        #plt.errorbar(step_sizes[1:], bs[1:], b_errs[1:], fmt="*")
        plt.errorbar(step_sizes[1:], taus[1:], tau_errs[1:], fmt="*")
        plt.xlabel("Step size in detector [V]")
        plt.title(r"Lifetime $ \tau $ vs Step size in detector")
    
        should_fit_taus = False
        if should_fit_taus:
            fit_taus(step_sizes, taus)
    else:
        # voltages = [0.4, 0.8, 2]
        voltages = [1.5, 1.3, 1.1, 1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05]
        plt.errorbar(voltages, taus, tau_errs, fmt="*")
        #plt.errorbar(voltages, bs, b_errs, fmt="*")
        plt.xlabel("Voltage on LED [V]")
        plt.title(r"Lifetime $ \tau $ vs Voltage on LED")


# %%
def fit_taus(step_sizes, taus):
    sqrt_func = lambda step, A, C0, X0: A*np.sqrt(step-X0)+C0
    popt, pcov = curve_fit(sqrt_func, step_sizes, taus, p0=(1e-5, 8e-6, 0), maxfev=10000)
    params_errors = np.sqrt(np.diag(pcov))
    print_params(["A", "C0", "X0"], popt, params_errors)
    
    xx = np.arange(np.min(step_sizes),np.max(step_sizes), 0.05)
    plt.plot(xx, sqrt_func(xx, *popt), "--", label="Fit to sqrt")
    plt.legend()
        


# %% Rise time example
def plot_rise_time_example():
    path = r"..\results_W3\8-blame_who-7.2cm-400mV-400KHZ.txt"
    xx, y = load_data(path)
    
    # Fix the time vector, due to low resolution in saved data
    dx = np.diff(xx)[0]
    x = np.arange(xx[0], xx[-1]+dx, dx)
    
    ignore_up_factor = 0.95
    ignore_down_factor=0.01
    some_high_index = np.where(y > y.max()*ignore_up_factor)[0][0]
    y1 = y[some_high_index:]
    x1 = x[some_high_index:]
    slope_start_index = np.where(y1 < y1.max()*ignore_up_factor)[0][0]  - 2 
    slope_end_index = np.where(y1 < (y1.min()+np.abs(y1.min()*ignore_down_factor)))[0][0]
    
    x2 = x1[slope_start_index:slope_end_index]
    x2 -= x2[0]  # Start from 0
    y2 = y1[slope_start_index:slope_end_index]
    
    fig, ax = plt.subplots()
    ax.plot(x2,y2, '.')
    ax.set_title("Rise time example")
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Voltage [V]")    
    
    
def plot_example2():
    path = r"..\results_W4\4-blamewho1\1-5.4rt.txt"
    fit_to_exp(path)
    fig = plt.figure(path)
    plt.title("V vs Time showing step response")
    plt.xlim(7e-4,8.5e-4)
    plt.ylim(-2.5,2.5)
    
    axs = plt.axes()
    del axs.lines[1]
    fig.show()
    
    # %% 
def main():
    paths1  = [r"..\results_W3\8-blame_who-7.2cm-400mV-400KHZ.txt", 
              r"..\results_W3\9-blame_who-8.8cm-800mV-400KHZ.txt",
              r"..\results_W3\10-blame_who-11.3cm-2V-400KHZ.txt"]
    
    paths2  = [r"..\results_W3\11-blame_who_way2-2V-400KHZ-24cm.txt", 
              r"..\results_W3\12-blame_who_way2-2V-400KHZ-15cm.txt",
              r"..\results_W3\13-blame_who_way2-2V-400KHZ-9.1cm.txt", 
              r"..\results_W3\14-blame_who_way2-2V-400KHZ-6.1cm.txt"]
    
    paths3 = glob.glob(r"..\results_W4\4-blamewho1\*rt.txt")
    paths4 = glob.glob(r"..\results_W4\5-blamewho2\*rt.txt")
    
    #plot_rise_time_example()
    # plot_example2()
    #analyze(paths2, by_step_size=True)
    #analyze(paths1, by_step_size=False)
    analyze(paths3, by_step_size=True, should_plot=False)
    #analyze(paths4, by_step_size=False, should_plot=False)

if __name__ == "__main__":
    plt.close("all")
    mpl.rcParams['font.size'] = 16
    main()

import pandas as pd
import numpy as np 
from numpy import pi 
import matplotlib.pyplot as plt 


def gen_rand_data(y0=0, t0=0):
    T = 5  # Measurement duration 
    F = 500  # Measurement frequency 
    d = 1/F  # duration between measurements 
    N = int(T*F)  # Number of measurements 
    
    x = np.linspace(0, T, T*F)  # Timestamps of samples 
    f1 = 15
    f2 = 200
    omega1 = (2*pi)*f1
    omega2 = (2*pi)*f2
    y = np.cos(omega1*x)+1*np.sin(omega2*x)  # Sampled data 
    
    # Add noise 
    a = 10
    y += a*(np.random.rand(N)-0.5)
    y += y0
    y[:t0] = 0
    y[-t0:] = 0
    
    return x, y, N, d


def gen_rect():
    T = 100  # Measurement duration 
    F = 500  # Measurement frequency 
    d = 1/F  # duration between measurements 
    N = int(T*F)  # Number of measurements 
    
    x = np.linspace(0, T, T*F)  # Timestamps of samples 
    y = 0 * x
    middle = len(y)//2
    L = len(y)//10
    y[middle-L:middle+L] = 1
    
    
    return x, y, N, d


def gen_delta(x0):
    T = 100  # Measurement duration 
    F = 500  # Measurement frequency 
    d = 1/F  # duration between measurements 
    N = int(T*F)  # Number of measurements 
    
    x = np.linspace(0, T, T*F)  # Timestamps of samples 
    y = 0 * x
    y[x0] = 1000    
    
    return x, y, N, d


def gen_am(mod_freq=1000):
    T = 1  # Measurement duration 
    F = 5000  # Measurement frequency 
    d = 1/F  # duration between measurements 
    N = int(T*F)  # Number of measurements 
    
    carrier_freq = mod_freq
    w = 2*np.pi*carrier_freq

    x = np.linspace(0, T, T*F)  # Timestamps of samples     
    
    A = 0.2
    W=0.5**6
    signal = A*np.exp(-((x-0.5)/W)**2)
    am = np.sin(w*x)     
    y =  signal * am
    # plt.plot(x,y)
    
    return x, y, N, d


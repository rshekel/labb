# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 17:30:18 2019

@author: ronen
"""
import numpy as np 
from numpy import pi 
import matplotlib.pyplot as plt 
import glob

from wavegen import *


def load_data(path):
    data = np.loadtxt(path, skiprows=8)
    x = data[:, 0]
    y =  data[:, 1]
    return x, y 

def plot_signal(x, y, title, x2=None, y2=None):
    plt.figure(title)
    plt.title(title)
    plt.plot(x, y)
    
    if x2 is not None and y2 is not None:
        plt.plot(x2, y2)

def normalize_to_zero(y):
    return y - y.mean()


def calc_fourier(x, y, N, d):
    fa = np.fft.fft(y)
    freqs = np.fft.fftfreq(N, d)    
    return fa, freqs
        

def plot_fourier(fa, freqs, abs_val=True, pos_mask=True, title=None):
    fig_title = title or "Fourier transform"
    plt.figure(title)
    plt.title(fig_title)

    if pos_mask:
        mask = freqs > 0
        freqs = freqs[mask]
        fa = fa[mask]

    if abs_val:
        plt.plot(freqs, abs(fa))
    else:
        plt.plot(freqs, fa.real, freqs, fa.imag)    

    plt.xlabel("Freq [Hz]")
    #plt.yscale("log")


def fourier_to_pure_sig(fa, good_freq, good_freq_range=10):
    fa[:good_freq-good_freq_range] = 0    
    fa[good_freq+good_freq_range:] = 0

    return fa 

def fourier_to_less_noise(fa, freqs, percent_is_noise=0.5, min_freq=500):
    
    # Ignore noise below min_freq - it is noise 
    mask = np.abs(freqs) < min_freq
    fa[mask] = 0
    
    # ignore small-ampl. noises
    max_freq = np.max(np.abs(fa))
    mask = np.abs(fa) < (percent_is_noise * max_freq)
    fa[mask] = 0
 
    return fa
    
def move_to_am(fa, mod_freq):
    fa = np.fft.fftshift(fa)
    fa[:-mod_freq] = fa[mod_freq:]
    fa[-mod_freq:] = 0
    fa = np.fft.fftshift(fa)
    return fa

def fix_darkness(fa):
    # assuming len of 40000
    path = "..\\results_W2\\29-dark.txt"
    x, y = load_data(path)
    dark_fa, dark_freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    return fa + dark_fa
    
    
def analyze1(x, y, sig_freq, sig_freq_range=10, title="Original signal"):
    
    plot_signal(x, y, title=title)
    #plt.xlim([0.5,0.55])
    y = normalize_to_zero(y)
    fa, freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    plot_fourier(fa, freqs, abs_val=True)
    fa = fourier_to_less_noise(fa, freqs, 0.1, 110)
    clean_sig = np.fft.ifft(fa)
    plot_signal(x, y, "Pure signal", x, clean_sig)
    #plt.xlim([0.5,0.55])
    

def analyze2(x, y, mod_freq, freq_range=400):
    plot_signal(x, y, title="Original signal")
    y = normalize_to_zero(y)
    fa, freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    plot_fourier(fa, freqs, abs_val=True, pos_mask=False)
    # clean_sig = fourier_to_less_noise(fa, freqs, 0.2, 1)
    #fa = fix_darkness(fa)
    fa = fourier_to_pure_sig(fa, mod_freq, freq_range)
    
    fa = move_to_am(fa, mod_freq)
    
    clean_sig = np.fft.ifft(fa)
    
    plot_fourier(fa, freqs, abs_val=True, pos_mask=False)
    # Multiplying by 2 because we clean the negative freqs
    plot_signal(x, y, "only gausian", x, 2*abs(clean_sig))
    

plt.close('all')
#mod_freq = 1000
#x, y, N, d = gen_am(mod_freq)
#analyze2(x,y, mod_freq)    
#x, y, N, d = gen_rand_data(5, 300)

#path = "../results/16.txt"
results_folder = "..\\results_W3\\"
path = glob.glob(results_folder+"*.txt")[-1]
#path = "..\\results_W2\\28-gaus2e-12-3k-30cm.txt"
x, y = load_data(path)
#analyze2(x, y, 0)
plt.plot(x,y,".")
#plt.xlim(0.499, 0.501)


#analyze2(x, y, 3000, 40)


def find_y_resolution(y):
    diffs = np.abs(np.diff(y))
    diffs = diffs[diffs>0]
    return np.min(diffs)
# -*- coding: utf-8 -*-
from fourier import *
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, mark_inset

from fit_rsqr import * 
from scipy.optimize import curve_fit


results_folder = "..\\results_W2\\"

def plot_saw_tooth():
    file_name = "07-3k-sawtooth-25cm.txt"
    path = results_folder + file_name
    
    x, y = load_data(path)
    analyze1(x, y, -1, -1)
    
    # Edit the graphs to be human readable
    plt.figure("Original signal")
    plt.xlim(0, 0.004)
    
    # Add peaks equation
    plt.figure("Fourier transform")
    A = 52*3000
    xx = np.arange(2500, 40e3, 1)
    yy = A/xx
    plt.plot(xx, yy, "--")
    
    
def plot_sin_wave():
    file_name = "04-6.4K-30mV-more_samples.txt"
    path = results_folder + file_name
    
    x, y = load_data(path)
    analyze1(x, y, -1, -1, title="Original Signal - sin wave f=6.4KHz")
    
    # Edit the graphs to be human readable
    plt.figure("Original Signal - sin wave f=6.4KHz")
    plt.xlim(0, 0.002)
    plt.xlabel("Time [s]")
    plt.ylabel("Amplitude [V]")
    
def complex_demod_sin_sin():
    path = "..\\results_W2\\19-sin188sin3k-10mv-30cm.txt"
    # path = "..\\results_W2\\17-sin30sin3k-10mv-15cm.txt"
    x, y = load_data(path)
    analyze2(x, y, 3000)

def plot_sqr():
    path = "..\\results_W2\\12-3k-sqr-10cm.txt"
    x, y = load_data(path)
    plot_signal(x, y, title="Original signal")
    #plt.xlim([0.5,0.55])
    y = normalize_to_zero(y)
    fa, freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    plot_fourier(fa, freqs)
    fa = fourier_to_less_noise(fa, freqs, 0.05, 110)
    plot_fourier(fa, freqs)
    clean_sig = np.fft.ifft(fa)
    plot_signal(x, y, "Pure signal", x, clean_sig)
    #plt.xlim([0.5,0.55])
    

def plot_gaussian():
    # %% Start
    # here sigma is 2**-12 
    path = "..\\results_W2\\26-gaus2e-12-3k-10cm.txt"
    
    #path = "..\\results_W2\\27-gaus2e-12-3k-50cm.txt"
    
    #path = "..\\results_W2\\25-gaus2e-10-3k-10cm.txt"
    x, y = load_data(path)

    # DELETE THIS!
    t = x
    a = 0.0003
    w = 3000 * 2*np.pi  # Should we multiply by 2 pi?
    exp = np.exp
    sin = np.sin
    y = a*exp(-((t-0.5)**2)*2**12)*sin(w*(t-0.5))
    y += (np.random.rand(len(x))-0.5) * 0.0005
    

    plot_signal(x, y, title="Small Gaussian - Mostly noise")
    plt.xlim([0.4,0.6])
    plt.xlabel("Time [s]")
    plt.ylabel("Voltage [V]")
    
    y = normalize_to_zero(y)
    fa, freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    title = "Fourier transform of Gaussian using AM"
    plot_fourier(fa, freqs, title=title)
    
    # Zoom on the gaussian
    #plt.xlim(2850, 3150)
    #plt.ylim(0,4)
    fig = plt.figure(title)
    ax = fig.axes[0]
    zoomed_ax = ax.inset_axes([0.15, 0.2, 0.8, 0.7])    
    zoomed_ax.set_xlim(3000-150, 3000+150)
    #zoomed_ax.set_ylim(0, 0.13)
    
    # Specifying the indexes because there is a wierd line that crosses the graph
    zoomed_ax.plot(freqs[3000-150: 3000+150], abs(fa[3000-150: 3000+150]), 'g', label="Zoomed to the gaussian")
    zoomed_ax.legend()
    mark_inset(fig.axes[0], zoomed_ax, loc1=3, loc2=4, fc="None", ec="r", ls='--', lw=2)
    
    carrier_freq = 3000
    fa = fourier_to_pure_sig(fa, carrier_freq, good_freq_range=70) # Cleaning 350 because empirically this gives the best result
    fa = move_to_am(fa, carrier_freq)

    clean_sig = np.fft.ifft(fa)
    plot_signal(x, y, "Pure signal - Gaussian")    
    
    plot_fourier(fa, freqs, pos_mask=False, title="Clean fourier")
    plt.xlim(-100,100)
    
    # %% Fit to gaussian
    should_fit_fourier = True
    if should_fit_fourier:
        gaussian_func = lambda f, A, W, C: C+A*np.exp(-f**2/(2*W**2)) 
        
        start_A = 3
        start_W = 30
        
        abs_fa = np.abs(fa)
        
        popt, pcov = curve_fit(gaussian_func, freqs, abs_fa, p0=(start_A, start_W, 0), maxfev=10000)
        params_errors = np.sqrt(np.diag(pcov))
        print_params(["A", "W"], popt, params_errors)
        
        xx = np.arange(np.min(freqs),np.max(freqs), 0.2)
        plt.plot(xx, gaussian_func(xx, *popt), "--", label="Fit to gaussian")
        plt.legend()
        

        zoomed_ax.plot(xx + carrier_freq, gaussian_func(xx, *popt), "--", linewidth=2.5, label="Fit to gaussian")
        zoomed_ax.legend()
    # %% Plot clean signal
    clean_sig = np.fft.ifft(fa)
    clean_sig = 2*abs(clean_sig)
    plot_signal(x, clean_sig, "Pure signal - Gaussian")
    
    plt.xlabel("Time [s]")
    plt.ylabel("Voltage [V]")
    plt.xlim(0.4,0.6)
    
    # %% Fit to clean signal
    
    start_A = 0.005
    start_W = 0.01
    start_t0 = 0.5
    gaussian_func = lambda t, A, W, t0: A*np.exp(-(t-t0)**2/(2*W**2)) 
    
    popt, pcov = curve_fit(gaussian_func, x, clean_sig, p0=(start_A, start_W, start_t0))
    params_errors = np.sqrt(np.diag(pcov))
    print_params(["clean_fit A", "clean_fit W", "clean_fit t0"], popt, params_errors)
    
    title = "Fit of clean signal - Gaussian"
    plt.figure(title)
    plt.title(title)
    plt.plot(x, clean_sig, label="clean signal")
    xx = np.arange(np.min(x),np.max(x), 0.001)
    plt.plot(xx, gaussian_func(xx, *popt), "--", label="Fit to gaussian", linewidth=0.7)
    plt.legend()
    plt.xlabel("Time [s]")
    plt.ylabel("Volage [V]")
    
    # Plot fourier for the original gaussian
    #fa, freqs = calc_fourier(xx,gaussian_func(xx, *popt),len(xx), xx[1]-xx[0])
    #plot_fourier(fa, freqs, abs_val=True)
    
    # %% Calcs
    original_w = np.sqrt(1/(2*2**12))
    print("Original W={w}".format(w=original_w))
    w = popt[1]
    w_err = params_errors[1]
    print("Error of {}".format(float(original_w-w)/w_err))
    print("Percentage error: {}%".format(np.abs(w-original_w)/original_w*100))
    # %%
    
def extract_gaussian(x, y, freqs, fa, carrier_freq, ax):
    fa = fa.copy()  # Create a copy - instead of changing the original
    fa = fourier_to_pure_sig(fa, carrier_freq, good_freq_range=70)
    fa = move_to_am(fa, carrier_freq)

    clean_sig = np.fft.ifft(fa)
    plot_signal(x, y, "Pure signal - Gaussian")    
    
    plot_fourier(np.fft.fftshift(fa), np.fft.fftshift(freqs), pos_mask=False, title="Clean fourier")
    plt.xlim(-100,100)
    # %% Plot clean signal
    clean_sig = np.fft.ifft(fa)
    clean_sig = 2*abs(clean_sig)
    plot_signal(x, clean_sig, "Pure signal - Gaussian")
    
    plt.xlabel("Time [s]")
    plt.ylabel("Voltage [V]")
    plt.xlim(0.4,0.6)
    
    # %% Fit to clean signal
    
    start_A = 0.005
    start_W = 0.01
    start_t0 = 0.5
    gaussian_func = lambda t, A, W, t0: A*np.exp(-(t-t0)**2/(2*W**2)) 
    
    popt, pcov = curve_fit(gaussian_func, x, clean_sig, p0=(start_A, start_W, start_t0))
    params_errors = np.sqrt(np.diag(pcov))
    print_params(["clean_fit A", "clean_fit W", "clean_fit t0"], popt, params_errors)
    
    title = "Fit of clean signal - Gaussian"
    ax.set_title(title)
    ax.plot(x, clean_sig, label="clean signal")
    xx = np.arange(np.min(x),np.max(x), 0.001)
    ax.plot(xx, gaussian_func(xx, *popt), "--", label="Fit to gaussian", linewidth=0.7)
    ax.legend()
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Volage [V]")
    
    # Plot fourier for the original gaussian
    #fa, freqs = calc_fourier(xx,gaussian_func(xx, *popt),len(xx), xx[1]-xx[0])
    #plot_fourier(fa, freqs, abs_val=True)
    
    return popt, params_errors
    # %%
    
def plot_3_gaussians():
    # %%
    # Data:
    #path = r"..\results_W4\7-triplegaus\1.txt"
    path = r"..\results_W4\7-triplegaus\3.txt"
    #path = r"..\results_W4\7-triplegaus\2.txt"
    
    #path = r"..\results_W4\8-triplegaus2\1.txt"
    #path = r"..\results_W4\8-triplegaus2\2.txt"
    
    f = 3e3
    freq1 = f
    freq2 = 2*f
    freq3 = 3*f
    #freq2 = 1.33*f
    #freq3 = 3.12*f
    
    
    xx, y = load_data(path)
    
    # Fix the time vector, due to low resolution in saved data
    dx = np.diff(xx)[0]
    x = np.arange(xx[0], xx[-1]+dx, dx)
    
    # DELETE THIS!
    t = x
    a = 0.0003
    w = f * 2*np.pi  # Should we multiply by 2 pi?
    exp = np.exp
    sin = np.sin
    #y = a*exp(-((t-0.5)**2)*2**12)*sin(w*(t-0.5)) + 2*a*exp(-((t-0.5)**2)*2**11)*sin(2*w*(t-0.5)) + 5*a*exp(-((t-0.5)**2)*2**13)*sin(3*w*(t-0.5))
    #y += (np.random.rand(len(x))-0.5) * 0.0005
    

    plot_signal(x, y, title="3 Gaussians with different carrier frequency")
    plt.xlim([0.4,0.6])
    plt.xlabel("Time [s]")
    plt.ylabel("Voltage [V]")
    
    # %% Plot fourier with zoom in:
    y = normalize_to_zero(y)
    fa, freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    title = "Fourier transform of 3 gaussians using AM"
    plot_fourier(fa, freqs, title=title)
    
    fig = plt.figure(title)
    ax = fig.axes[0]
    carrier_fs = [freq1, freq2, freq3]
    colors = ['g', 'r', 'c']
    num_of_gaussians = len(carrier_fs)
    # Zoom on the gaussian
    """
    for i, carrier_f in enumerate(carrier_fs):
        zoomed_ax = ax.inset_axes([0.13+i/num_of_gaussians, 0.2, 0.85/num_of_gaussians, 0.75])    
        zoomed_ax.set_xlim(carrier_f-150, carrier_f+150)
        zoomed_ax.set_ylim(0, 18)
        
        cf = int(carrier_f)
        zoomed_ax.plot(freqs[cf-150: cf+150], abs(fa[cf-150: cf+150]), colors[i],
                       label="Gaussian {}, f={}Hz".format(i+1, carrier_f))
        zoomed_ax.legend()
        # This looks ugly, instead adding index on the original gaussian
        #mark_inset(fig.axes[0], zoomed_ax, loc1=3, loc2=None, fc="None", ec="r", ls='--', lw=2)
        
        #ax.annotate("{}".format(i+1),
        #    xy=(carrier_f, 0), xycoords='data',
        #    arrowprops=dict(facecolor=colors[i]),
        #    horizontalalignment='right', verticalalignment='top')
        ax.plot(carrier_f, -2, c=colors[i], marker="^", markersize=15)
    """
    # %% Fit
    expected = [2**12, 2**11, 2**13]
    
    #fig, axs = plt.subplots(num_of_gaussians)
    fig, axs = plt.subplots(1, num_of_gaussians, num="Fit to gaussians")
    fig.suptitle("Fit to gaussians")
    for i, carrier_f in enumerate(carrier_fs):
        popt, errs = extract_gaussian(x, y, freqs, fa, int(carrier_fs[i]), axs[i])
        axs[i].set_xlim(0.4,0.6)
        
        # Calc error
        original_w = np.sqrt(1/(2*expected[i]))
        print("Original W={ow}".format(ow=original_w))
        w = popt[1]
        w_err = errs[1]
        print("Error of {}".format(float(original_w-w)/w_err))
        print("Percentage error: {}%".format(np.abs(w-original_w)/original_w*100))

    # %%

def plot_darkness():
    file_name = "30-dark2.txt"
    path = results_folder + file_name
    
    x, y = load_data(path)
    print("Sample freq={}".format(1/(x[1]-x[0])))
    plot_signal(x, y, title="Voltage vs Time - no LED")    
    plt.xlabel("Time [s]")
    plt.ylabel("Volage [mV]")

    y = normalize_to_zero(y)
    fa, freqs = calc_fourier(x,y,len(y), x[1]-x[0])
    plot_fourier(fa, freqs, abs_val=True)
    plt.xlabel("Frequency [Hz]")
    plt.title("Fourier - no LED")

    plt.xlim(0,1000)

def main():
    plt.close('all')
    mpl.rcParams['font.size'] = 16
    # plot_saw_tooth()
    #plot_sin_wave()
    #complex_demod_sin_sin()
    # plot_sqr()
    #plot_gaussian()
    plot_3_gaussians()
    # plot_darkness()


if __name__ == "__main__":
    main()

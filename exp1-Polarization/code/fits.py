import numpy as np 
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def fit_to_cos_sqr(xdata, ydata, omega=1, start_A=None, start_phi=None):
    sin_sqr = lambda x, a, p: a*np.power(np.sin(omega*x-p), 2)
    
    start_A = np.max(ydata) if start_A is None else start_A
    if start_phi is None:
        index_of_min = np.argmin(ydata)
        start_phi = np.mod(xdata[index_of_min], np.pi)
        
    popt, pcov = curve_fit(sin_sqr, xdata, ydata, p0=(start_A, start_phi), bounds=((0, -np.pi), (np.inf, np.pi)))
    
    x = np.linspace(0, 2*np.pi, 200)
    y = sin_sqr(x, *popt)
    
    plt.plot(x, y, "r--", label=r"fit to $ A\cos^{2}\left(x-\phi\right) $")
    return popt, pcov, sin_sqr

def residuals(real_x, real_y, fit_func, *fit_func_params):
        plt.figure("Residuals")
        difference = fit_func(real_x, *fit_func_params) - real_y
        plt.plot(real_x, difference,'or')
        plt.grid()

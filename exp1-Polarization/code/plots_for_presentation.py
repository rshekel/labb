import numpy as np 
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt

from paths import * 
from fits import fit_to_cos_sqr, residuals

def plot():
    
    data = np.loadtxt(path_LC_over_volt, skiprows=1)
    
    volt = data[:, 0]
    volt *= 1000
    I =  data[:, 1]
    
    I_errs = I * 0.04
    
    plt.figure("Power over volt")
    plt.title("Power of light over voltage on LC", fontdict={'fontsize': 14})
    plt.errorbar(volt, I, I_errs, fmt='*r', ms=4)
    plt.xlabel("Volt (mV)", fontdict={'fontsize': 12})
    plt.ylabel(r"I ($\mu A$)", fontdict={'fontsize': 12})
    plt.grid()
        
    normalized_I = I/np.max(I)
    cos_phi = 1-2*normalized_I
    phi = np.arccos(cos_phi)
    
    plot_radians_vs_voltage(phi, volt)

    middle_range = np.logical_and(volt>1450 , volt<2450)
    old_phi = phi[middle_range]
    phi[middle_range] = 2*np.pi - phi[middle_range]
    plot_phi_transformation(phi, volt, old_phi, middle_range)
    
    old_phi = phi[volt<1450]
    phi[volt<1450] += 2*np.pi
    plot_phi_transformation(phi, volt, old_phi, volt<1450)
    
    plot_retardance_over_volt(phi, volt, normalized_I)

def plot_radians_vs_voltage(phi, volt):
    plt.figure("LC Retardance in radians over voltage");
    label = r"$\phi=\cos^{-1}\left(2I_{norm}-1\right)$"
    plt.plot(volt, phi, '.', label=label)
    subject = "LC Retardance in radians over voltage"
    plt.title(subject, fontdict={'fontsize': 16})
    plt.xlabel("Volt (mV)", fontdict={'fontsize': 16})
    plt.ylabel("Retardance (rad)", fontdict={'fontsize': 16})
    plt.legend(fontsize=14)
    plt.grid()


def plot_phi_transformation(phi, volt, old_phi, old_indexes):
    old_phi_volt = volt[old_indexes]
    
    plt.figure("LC Retardance [rads] vs Voltage [mV]");
    label = r"$\phi=\cos^{-1}\left(2I_{norm}-1\right)$"
    plt.plot(volt, phi, '.b', label=label)
    plt.plot(volt[old_indexes], phi[old_indexes], 'og', label=label)
    plt.plot(old_phi_volt, old_phi, 'xr')
    
    
    # Plot change lines
    for old_i, old_p in zip(np.where(old_indexes)[0], old_phi):
        x = volt[old_i]
        y1 = phi[old_i]
        y2 = old_p
        plt.plot([x,x], [y2,y1], '--c', lw=0.5)
    
    subject = "LC Retardance [rads] vs Voltage [mV]"
    plt.title(subject, fontdict={'fontsize': 16})
    plt.xlabel("Volt (mV)", fontdict={'fontsize': 16})
    plt.ylabel("Retardance (rad)", fontdict={'fontsize': 16})
    #plt.legend(fontsize=14)
    plt.grid()
    plt.show(block=True)


def plot_retardance_over_volt(phi, volt, normalized_I):
    wave_length = 633    
    retardance = phi/(2*np.pi)*wave_length
    errs = calc_retardance_errs(normalized_I, wave_length)
    
    
    plt.figure("Retardance over volt")
    plt.errorbar(volt, retardance, errs, fmt='.', lw=1.5, ms=0.9, label="Calculated retardance") 
    subject = "LC Retardance over voltage"
    plt.title(subject, fontdict={'fontsize': 16})
    plt.xlabel("Volt (mV)", fontdict={'fontsize': 16})
    plt.ylabel("Retardance (nm)", fontdict={'fontsize': 16})
    plt.legend(prop={'size': 14})
    plt.grid()


def calc_retardance_errs(I_norm, wave_length):
    return wave_length/(2*np.pi) * 0.04* (I_norm / (np.sqrt(I_norm-I_norm**2)))


def main():
    plt.close("all")
    mpl.rcParams['font.size'] = 8
    plot()


if __name__ == "__main__":
    main()
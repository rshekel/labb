import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt

from paths import * 
from fits import fit_to_cos_sqr, residuals


def plot_retardance_over_volt():
    
    data = np.loadtxt(path_LC_over_volt, skiprows=1)
    
    volt = data[:, 0]
    volt *= 1000
    I =  data[:, 1]
    
    I_errs = I * 0.04
    
    plt.figure("Power over volt")
    plt.title("Power of light over voltage on LC", fontdict={'fontsize': 18})
    plt.errorbar(volt, I, I_errs, fmt='*r')
    plt.xlabel("Volt (V)", fontdict={'fontsize': 16})
    plt.ylabel(r"I ($\mu A$)", fontdict={'fontsize': 16})
    plt.grid()
    
    normalized_I = I/np.max(I)
    cos_phi = 1-2*normalized_I
    phi = np.arccos(cos_phi)
    
    plt.figure("LC Retardance in radians over voltage");
    label = r"$\phi=\cos^{-1}\left(2I_{norm}-1\right)$"
    plt.plot(volt, phi, '.', label=label)
    subject = "LC Retardance in radians over voltage"
    plt.title(subject, fontdict={'fontsize': 16})
    plt.xlabel("Volt (mV)", fontdict={'fontsize': 16})
    plt.ylabel("Retardance (rad)", fontdict={'fontsize': 16})
    plt.legend(fontsize=14)
    plt.grid()
    
    #phi[volt>2450] *= -1
    #phi[volt<1450] = 2*np.pi - phi[volt<1450]
    #phi += np.pi
    
    phi[volt<1450] += 2*np.pi
    middle_range = np.logical_and(volt>1450 , volt<2450)
    phi[middle_range] = 2*np.pi - phi[middle_range]
    
    wave_length = 633
    
    retardance = phi/(2*np.pi)*wave_length
    
    errs = calc_retardance_errs(normalized_I, wave_length)
    
    plt.figure("Retardance over volt")
    plt.errorbar(volt, retardance, errs, fmt='.', label="Calculated retardance") 
    subject = "LC Retardance over voltage"
    plt.title(subject, fontdict={'fontsize': 16})
    plt.xlabel("Volt (mV)", fontdict={'fontsize': 16})
    plt.ylabel("Retardance (nm)", fontdict={'fontsize': 16})
    plt.legend(prop={'size': 14})
    plt.grid()
    
    
def calc_retardance_errs(I_norm, wave_length):
    return wave_length/(2*np.pi) * 0.04* (I_norm / (np.sqrt(I_norm-I_norm**2)))
    

def analyze_reflection():
    data = np.loadtxt(path_reflection, skiprows=1)
    theta_i = data[:, 0] / 180 * pi
    r_par = data[:, 1]
    r_perp =  data[:, 2]
    
    n_i = 1
    n_t = 1.6
    theta_t = np.arcsin(n_i/n_t*np.sin(theta_i))

    expected_R_par = (np.tan(theta_i-theta_t)**2)/(np.tan(theta_i+theta_t)**2)
    # We expect the ratio to be const ~ the incidence electric field
    r_par_ratio = r_par / expected_R_par
    
    expected_R_perp = (np.sin(theta_i-theta_t)**2)/(np.sin(theta_i+theta_t)**2)
    r_perp_ratio = r_perp / expected_R_perp
    

def plot_power_over_angle_from_csv(path):
    data = np.loadtxt(path, skiprows=1)
    deg = data[:, 0]
    rad = (deg * np.pi)/180
    I = data[:, 1]
    # good_max = I.sort()[-2]
    # I_norm = I / np.max(I)  # No normalizing because we'll use fit to find I0
    errs = I * 0.04
    plt.errorbar(rad, I, errs, fmt='.')
    plt.plot(rad, I, '*b', label="Power from laser")
    
    plt.xlabel("Angle [rad]", fontdict={'fontsize': 16})
    plt.ylabel("I [mA]", fontdict={'fontsize': 16})
    
    return rad, I


def plot_keithley(path, polar_plot=True):
    plt.figure()
    df = pd.read_excel(path)
    t = df['Time [s]']
    t = t - t[0]
    I = df['Current [A]']
    I = I * 1000
    
    t = t[9:]
    I = I[9:]
    if polar_plot:
        theta = (t / t[len(t)-1]) * 2*np.pi
        plt.polar(theta, I, 'r*')
        plt.xlabel("Approx. Theta [Rad]", fontdict={'fontsize':22})
    else:
        plt.plot(t, I, 'r*')
        plt.xlabel("time [s]", fontdict={'fontsize':22})
    plt.title("Power from laser over time", fontdict={'fontsize':24})
    
    plt.ylabel("Current [mA]", fontdict={'fontsize':22})
    # mpl.rc_params.la
    return t, I


def plot_all():
    plt.close('all')
    plot_malus()
    plot_malus_3_polarizers()
    plot_half_wave()
    plot_quarter_plate_on_axis()
    plot_quarter_plate_circular_polarization()
    plot_quarter_plate_eliptical_polarization_over_time()


def plot_malus():
    plt.figure("Malus 2 polarizers")
    
    rad, I = plot_power_over_angle_from_csv(path_malus_1)
    subject = "Malus law - 2 polarizers. Rotating the 2nd"
    plt.title(subject, fontdict={'fontsize': 16})
    
    # plot_cos_sqr(2)
    popt, pcov, func = fit_to_cos_sqr(rad, I)
    plt.legend()

    residuals(rad, I, func, *popt)
    plt.title("residuals Graph for {subject}".format(subject=subject))
    

def plot_malus_3_polarizers():
    plt.figure("Malus 3 polarizers")
    subject = "Malus law - 3 polarizers. Rotating middle one"
    plt.title(subject, fontdict={'fontsize': 16})
    
    rad, I = plot_power_over_angle_from_csv(path_malus_2)
    #plot_half_wave_sin_sqr(0.42)
    popt, pcov, func = fit_to_cos_sqr(rad, I, omega=2)    
    plt.legend()
    
    residuals(rad, I, func, *popt)
    plt.title("residuals Graph for {subject}".format(subject=subject))


def plot_half_wave():
    plt.figure("Half wave plate")
    subject = "Half wave plate between 2 crossed polarizers. Rotating the plate"
    plt.title(subject, fontdict={'fontsize': 16})
    
    rad, I = plot_power_over_angle_from_csv(path_half_WL_plate)
    popt, pcov, func = fit_to_cos_sqr(rad, I, omega=2)    
    plt.legend()

    residuals(rad, I, func, *popt)
    plt.title("residuals Graph for {subject}".format(subject=subject))
    

def plot_quarter_plate_on_axis():
    plt.figure("Quarter wave plate - on axis")
    plot_keithley(path_quarter_WL_on_axis)
    plt.title("Quarter wave plate - plate on axis - rotating the polarizer", fontdict={'fontsize':24})


def plot_quarter_plate_circular_polarization():
    plt.figure("Quarter wave plate - circular polarization - rotating the polarizer")
    plot_keithley(path_circular_polarization)
    plt.title(r"Quarter wave plate - plate at $45^{\circ}$ - rotating the polarizer", fontdict={'fontsize':24})
    

def plot_quarter_plate_eliptical_polarization_over_time():
    plt.figure("Quarter wave plate - elliptical polarization - rotating the polarizer")
    plot_keithley(path_eliptical_polarization1)
    plt.title(r"Quarter wave plate - plate at $\approx22^{\circ}$ - rotating the polarizer", fontdict={'fontsize':24})
    

# This is a bad measurement! The measures should be at 1/1.5Hz but they are at about 1/1.82Hz 
def plot_quarter_plate_eliptical_polarization_over_angle():
    plt.figure("Quarter wave plate - elliptical polarization - rotating the polarizer")
    plot_keithley(path_eliptical_polarization2)
    plt.title(r"Quarter wave plate - plate at $\approx22^{\circ}$ - rotating the polarizer", fontdict={'fontsize':24})
    

def plot_cos_sqr(phi):
    plt.figure()
    x = np.linspace(0, 2*np.pi, 200)
    y = np.power(np.cos(x-phi+np.pi), 2)
    plt.polar(x, y, 'r')
    
    
def plot_half_wave_sin_sqr(phi):
    x = np.linspace(0, 2*np.pi, 200)
    y = np.power(np.sin(2*(x-phi+np.pi)), 2)
    plt.plot(x, y, 'r-')


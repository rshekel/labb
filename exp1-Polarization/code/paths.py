


# Week 1
# These 3 measurements are of lser over time to check stability 
# The frequency is of 5 minutes. measuring here through polarizer
path1 = r'../results/laser_one_oscilation_good.xlsx'

# Week 2 
# Frequency is of 50 sec. measuring here through polarizer
path2 = r'../results/laser_one_oscilation_20min.xlsx'

# Frequency is of 50 sec. measuring here straight from laser to detector
path3 = r'../results/laser_one_oscilation_no_polarizer.xlsx'
# t, I = plot_keithley(path1)

# Malus Law. first polarizer in place. another polarizer rotating. 
path_malus_1 = r'../results/malus01.txt'

# Malus Law. first polarizer in place. another polarizer at 90 deg. Third polarizer in middle rotating.  
path_malus_2 = r'../results/malus02.txt'

# Half WL plate. two polarizers at 90 deg. half plate in middle rotating (does shikuf to its axis)
path_half_WL_plate = r'../results/half_wave_plate.txt'

# Quarter plate. putting it at different angles - and then recording while changing by hand slowly the third polarizer. 
# when on axis - it shoudln't do anything - and be like cos^2 - from min to max. when 45deg - should be circular and stay in place. 
# in the middle - don't get to min or max, and have something wierd... eliipse? 
path_quarter_WL_on_axis = r'../results/quarter_plate_on_axis.xlsx'

path_circular_polarization = r'../results/quarter_circular.xlsx'

path_eliptical_polarization1 = r'../results/quarter_eliptic.xlsx'

# here we syncronized a measurement every 1.5 seconds, and changed by 10deg. supposed to be OK and elliptic. 
path_eliptical_polarization2 = r'../results/quarter_eliptic_1.5sec.xlsx'

path_reflection = r"../results/reflection_from_dialectric.txt"

path_LC_over_volt = "../results/LC_power_over_volt.txt"
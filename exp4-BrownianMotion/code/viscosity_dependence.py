import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import uncertainties

from analyze_tracker_file import Meas, K, ROOM_TEMP, get_all_csvs, pixel_to_meter

ROOT_FOLDER = r'..\data\week2'


class DependenceAnalyzer(object):
    MIN_R = pixel_to_meter(9)
    MAX_R = pixel_to_meter(13)

    def __init__(self, root_path, normalize_radius):
        paths = get_all_csvs(root_path)
        meass = [Meas(path) for path in paths]

        meass = [meas for meas in meass if DependenceAnalyzer.MIN_R < meas.radius < DependenceAnalyzer.MAX_R]
        meass = [meas for meas in meass if ROOM_TEMP - 3 < meas.temperature < ROOM_TEMP + 3]

        self.meass = meass
        self.viscosities = np.array([meas.viscosity for meas in meass])
        self.viscosities_vals = np.array([v.n for v in self.viscosities])
        self.radiuses = np.array([meas.radius for meas in meass])
        self.slopes = np.array([meas.slope for meas in meass])
        self.slopes_errs = np.array([meas.slope_err for meas in meass])

        self.normalize_radius = normalize_radius
        if normalize_radius:
            # Since we already 'confirmed' the dependency of the slope vs radius
            # we can normalize the slopes (as if radius=1 in all measurements)
            self.slopes *= self.radiuses
            self.slopes_errs *= self.radiuses
            base_radius_val = 1
            base_radius_err = 0.1
        else:
            base_radius_val = np.mean(self.radiuses)
            base_radius_err = np.std(self.radiuses * 1.1)
        self.base_radius = uncertainties.ufloat(base_radius_val, base_radius_err)

        self.fit_slope, self.fit_c = self._linear_fit()
        self.T = uncertainties.ufloat(ROOM_TEMP, 3)
        self.expected_slope = self._calc_expected_slope()

    def _linear_func(self, x, m, c):
        return m * x + c

    def _linear_fit(self, with_c=True):
        X = 1 / self.viscosities_vals
        Y = self.slopes

        if with_c:
            bounds = (-np.inf, np.inf)
        else:
            bounds = ([-np.inf, -1e-24], [np.inf, 1e-24])

        popt, pcov = curve_fit(self._linear_func, X, Y, bounds=bounds)
        fit_slope, fit_c = popt
        fit_slope_err, fit_c_err = np.sqrt(np.diag(pcov))

        slope = uncertainties.ufloat(fit_slope, fit_slope_err)
        c = uncertainties.ufloat(fit_c, fit_c_err)
        return slope, c

    def plot_dependency(self, ax):
        vs = 1 / self.viscosities
        xs = [v.n for v in vs]
        xs_errs = [v.s for v in vs]
        ax.errorbar(xs, self.slopes, self.slopes_errs, xs_errs, fmt='.', label='slopes')

        if self.normalize_radius:
            ax.set_title('Slope vs 1/Viscosity; Normalized by radius')
            ax.set_xlabel(r'1/Viscosity [$ kg^{-1}\cdot m\cdot s $]')
            ax.set_ylabel('Slope [$ m^{3}/s $]')
        else:
            ax.set_title('Slope vs 1/Viscosity')
            ax.set_xlabel(r'1/Viscosity [$ kg^{-1}\cdot m\cdot s $]')
            ax.set_ylabel('Slope [$ m^{2}/s $]')

    def plot_expected(self, ax):
        xs = np.array(ax.get_xlim())

        slope = self.expected_slope
        slope1 = slope.n + slope.s
        ys = self._linear_func(xs, slope1, 0)
        ax.plot(xs, ys, '--r', label='expected')

        slope2 = slope.n - slope.s
        ys = self._linear_func(xs, slope2, 0)
        ax.plot(xs, ys, '--r', label='expected')

    def plot_expected_simple(self, ax):
        xs = np.array(ax.get_xlim())
        slope = self.expected_slope.n
        ys = self._linear_func(xs, slope, 0)
        ax.plot(xs, ys, label='expected')

    def plot_fit(self, ax):
        xs = np.array(ax.get_xlim())
        ys = self._linear_func(xs, self.fit_slope.n, self.fit_c.n)
        ax.plot(xs, ys, '--', label='linear fit')

    def calc_n_sigma(self):
        diff = self.expected_slope.n - self.fit_slope.n
        err = np.sqrt(self.expected_slope.s ** 2 + self.fit_slope.s ** 2)
        return diff / err

    def print_results(self):
        print(f'expected slope: {self.expected_slope}')
        print(f'Viscosity dependence fit results: '
              f'{self.fit_slope} c={self.fit_c}')
        print(f'n sigma={self.calc_n_sigma()}')

    def print_results_latex(self):
        print(f'expected=\t\t{self.expected_slope:L}')
        print(f'fit_slope=\t\t{self.fit_slope:L}')
        print(f'fit_c=\t\t{self.fit_c:L}')
        print(r'n_{sigma}\approx ' + str(self.calc_n_sigma()))

    def analyze(self):
        self.print_results()
        fig, ax = plt.subplots()
        self.plot_dependency(ax)
        self.plot_expected(ax)
        self.plot_fit(ax)
        ax.legend()
        plt.show()

    def _calc_expected_slope(self):
        expected_linear_slope = 2 * K * self.T / (3 * np.pi * self.base_radius)
        return expected_linear_slope


def main():
    folders = [r'..\data\week1', r'..\data\week2']
    analyzer = DependenceAnalyzer(None, False)
    analyzer.analyze()


if __name__ == '__main__':
    main()

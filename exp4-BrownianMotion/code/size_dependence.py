import os
import glob

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import uncertainties

from analyze_tracker_file import Meas, linear_func, K, ROOM_TEMP

GLICEROL_18_PERCENT_FOLDER = '..\\data\\week1\\try2-several-sizes\\*.csv'
WATER_FOLDER = '..\\data\\week1\\try5-several-sizes\\*csv'


class DependenceAnalyzer(object):
    def __init__(self, paths):
        meass = [Meas(path) for path in paths]
        self.meass = meass
        self.slopes = np.array([meas.slope for meas in meass])
        self.slopes_errs = np.array([meas.slope_err for meas in meass])
        self.names = [os.path.basename(meas.path) for meas in meass]
        self.radiuses = np.array([meas.radius for meas in meass])
        self.radiuses_errs = 0.15 * self.radiuses

        self.fit_slope, self.fit_c, self.fit_slope_err, self.fit_c_err = self._linear_fit()

        # using first meas, because it should be the same in all meass
        self.viscosity = self.meass[0].viscosity
        self.T = uncertainties.ufloat(ROOM_TEMP, 3)
        self.expected_slope = self._calc_expected_slope()

    def _linear_func(self, x, m, c):
        return m * x + c

    def _linear_fit(self, with_c=True):
        X = 1 / self.radiuses
        Y = self.slopes
        if with_c:
            bounds = (-np.inf, np.inf)
        else:
            bounds = ([-np.inf, -1e-24], [np.inf, 1e-24])
        popt, pcov = curve_fit(self._linear_func, X, Y, bounds=bounds)

        fit_slope, fit_c = popt
        fit_slope_err, fit_c_err = np.sqrt(np.diag(pcov))
        return fit_slope, fit_c, fit_slope_err, fit_c_err

    def plot_all_lin_fits(self, ax):
        for i in range(len(self.names)):
            ax.plot([0, 1], linear_func(np.array([0, 1]), self.slopes[i]), '--', label=self.names[i])
        ax.legend()

    def plot_dependency(self, ax):
        radius_with_errs = [uncertainties.ufloat(r, r_err) for r, r_err in zip(self.radiuses, self.radiuses_errs)]
        radius_with_errs = np.array(radius_with_errs)
        xs_and_errs = 1 / radius_with_errs
        xs = [x.n for x in xs_and_errs]
        xs_errs = [x.s for x in xs_and_errs]

        ax.errorbar(xs, self.slopes, self.slopes_errs, xs_errs, fmt='.',
                    label='slopes')
        ax.set_title('Slope vs 1/Radius')
        ax.set_xlabel('1/a [1/m]')
        ax.set_ylabel('slope [$ m^{2}/s $]')

    def plot_expected(self, ax):
        expected_slope = self.expected_slope
        x_lim = np.array(ax.get_xlim())
        slope1 = expected_slope.n + expected_slope.s
        slope2 = expected_slope.n - expected_slope.s
        ax.plot(x_lim, linear_func(x_lim, slope1), '--r', label='expected')
        ax.plot(x_lim, linear_func(x_lim, slope2), '--r', label='expected')

    def plot_expected2(self, ax):
        x_lim = np.array(ax.get_xlim())
        expected_slope = self.expected_slope
        slope1 = expected_slope.n + expected_slope.s
        xs = np.linspace(x_lim[0], x_lim[1], 30)
        ys = linear_func(xs, expected_slope.n)
        ys_errs = linear_func(xs, slope1)-ys
        ax.errorbar(xs, ys, ys_errs)

    def plot_expected_simple(self, ax):
        xs = np.array(ax.get_xlim())
        slope = self.expected_slope.n
        ys = linear_func(xs, slope)
        ax.plot(xs, ys, label='expected')

    def plot_fit(self, ax):
        xs = np.array(ax.get_xlim())
        ys = self._linear_func(xs, self.fit_slope, self.fit_c)
        ax.plot(xs, ys, '--', label='linear fit')

    def calc_n_sigma(self):
        diff = self.expected_slope.n - self.fit_slope
        err = np.sqrt(self.expected_slope.s ** 2 + self.fit_slope_err ** 2)
        return diff / err

    def _calc_expected_slope(self):
        expected_slope = 2 * K * self.T / (3 * np.pi * self.viscosity)
        return expected_slope

    def print_results(self):
        print(f'expected slope: {self.expected_slope}')
        slope = uncertainties.ufloat(self.fit_slope, self.fit_slope_err)
        c = uncertainties.ufloat(self.fit_c, self.fit_c_err)
        print(f'Size dependence fit results: '
              f'slope={slope} c={c}')
        print(f'n sigma={self.calc_n_sigma()}')

    def analyze(self):
        self.print_results()
        fig, ax = plt.subplots()
        self.plot_dependency(ax)
        self.plot_fit(ax)
        self.plot_expected(ax)
        ax.legend()
        plt.show()


def main():
    # paths = glob.glob(GLICEROL_18_PERCENT_FOLDER)
    paths = glob.glob(WATER_FOLDER)

    analyzer = DependenceAnalyzer(paths)
    analyzer.analyze()


if __name__ == '__main__':
    main()

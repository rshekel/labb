import numpy as np 
import re
import uncertainties

ROOM_TEMP = 298.15  # Kelvin
ROOM_TEMP_C = 25
KELVIN_TO_CELSIUS = 273.15


def pixel_to_meter(pixels):
    return 0.1492e-6 * pixels


def load_data(path, fix=False):
    """returns T, X, Y vectors, fixed to mms"""

    data = np.loadtxt(path, delimiter=",", skiprows=2)
    T = data[:, 0]
    X = pixel_to_meter(data[:, 1])
    Y = pixel_to_meter(data[:, 2])

    X = X - X[0]
    Y = Y - Y[0]

    # Try and fix Schifa 
    if fix:
        v_x = (X[-1] - X[0]) / (T[-1] - T[0])
        v_y = (Y[-1] - Y[0]) / (T[-1] - T[0])

        X = X - (v_x * T)
        Y = Y - (v_y * T)
        print("Fixing schifa")

    return T, X, Y

def get_viscosity_from_glicerol(glicerol, temperature=ROOM_TEMP):
    viscosity_centipoise_dict = {}
    # shape should be:
    # viscosity_centipoise_dict[(<glicerol>, <temp>)]=(<viscosity>, <err>)
    # Units like in the paper, Celsius and Centipose
    viscosity_centipoise_dict[(0, ROOM_TEMP_C)] = (0.9, 0.1)
    viscosity_centipoise_dict[(0.181, ROOM_TEMP_C)] = (1.5, 0.2)
    viscosity_centipoise_dict[(0.455, ROOM_TEMP_C)] = (5, 1)
    viscosity_centipoise_dict[(0.686, ROOM_TEMP_C)] = (14, 3)
    viscosity_centipoise_dict[(0.9, ROOM_TEMP_C)] = (160, 35)
    viscosity_centipoise_dict[(0.09, ROOM_TEMP_C)] = (1.14, 0.07)
    viscosity_centipoise_dict[(0.317, ROOM_TEMP_C)] = (2.1, 0.4)

    viscosity_centipoise_dict[(0.09, 27.5)] = (1.1, 0.1)
    viscosity_centipoise_dict[(0.09, 47)] = (0.73, 0.07)
    viscosity_centipoise_dict[(0.09, 43)] = (0.79, 0.05)
    viscosity_centipoise_dict[(0.09, 33)] = (0.95, 0.05)
    viscosity_centipoise_dict[(0.09, 53)] = (0.64, 0.03)
    viscosity_centipoise_dict[(0.09, 58)] = (0.58, 0.01)

    viscosity_dict = {}  # In MKS
    for (glic, temp_c), (viscosity, std) in viscosity_centipoise_dict.items():
        key = (glic, temp_c + KELVIN_TO_CELSIUS)  # Change Celsius->Kelvin
        val = uncertainties.ufloat(viscosity, std)
        val /= 1000  # Change centipoise->Pa s
        viscosity_dict[key] = val

    if (glicerol, temperature) in viscosity_dict:
        return viscosity_dict[(glicerol, temperature)]
    else:
        raise Exception(
            f"Need to add to get_viscosity_from_glicerol function manually from paper table for "
            f"glicerol {glicerol} and temperature {temperature - KELVIN_TO_CELSIUS}C, {temperature}K")


def get_params_from_path(path):
    # mass_a_glic_98.3_r_22.5.csv
    match = re.match(r'.*glic_(.*)_r_(.+?)(_t_(.*))?\.csv.*', path)
    print(path)
    glic_percent = float(match.group(1)) / 100
    diameter = float(match.group(2))
    radius = pixel_to_meter(diameter) / 2

    temp = match.group(4)
    if temp is None:
        temp = ROOM_TEMP
    else:
        temp = float(temp) + KELVIN_TO_CELSIUS

    return glic_percent, radius, temp


def calc_real_glicerol(glic):
    # See ugly calculation in lyx file in main folder 
    factor = (1 / 1.26 - 1) / 9
    real_glic = glic / (10 / 9 + glic * factor)
    return round(real_glic, 3)

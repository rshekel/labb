import glob
import os

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import uncertainties

from analyze_tracker_file import Meas, linear_func, K

ROOT_FOLDER = r'..\data\week3\temperatures\*.csv'


class TempAnalyzer(object):
    def __init__(self, paths):
        meass = [Meas(path) for path in paths]
        self.meass = meass
        self.names = [os.path.basename(meas.path) for meas in meass]

        self.slopes = np.array([meas.slope for meas in meass])
        self.slopes_errs = np.array([meas.slope_err for meas in meass])
        slopes_ufloats = [uncertainties.ufloat(s, s_err) for s, s_err in zip(self.slopes, self.slopes_errs)]
        self.slopes_with_errs = np.array(slopes_ufloats)

        self.radiuses = np.array([meas.radius for meas in meass])
        self.radiuses_errs = 0.15 * self.radiuses
        rads_ufloats = [uncertainties.ufloat(r, r_err) for r, r_err in zip(self.radiuses, self.radiuses_errs)]
        self.radiuses_with_errs = np.array(rads_ufloats)

        self.viscosities_with_errs = np.array([meas.viscosity for meas in meass])
        self.viscosities = np.array([v.n for v in self.viscosities_with_errs])

        self.base_radius = np.mean(self.radiuses)
        self.Ts = np.array([meas.temperature for meas in meass])

        self.fit_slope, self.fit_c = self._linear_fit()
        self.expected_slope = self._calc_expected_slope()

    def _linear_func(self, x, m, c):
        return m * x + c

    def _linear_fit(self, with_c=True):
        X = self.Ts
        Y = self.slopes * self.radiuses * self.viscosities
        if with_c:
            bounds = (-np.inf, np.inf)
        else:
            bounds = ([-np.inf, -1e-24], [np.inf, 1e-24])
        popt, pcov = curve_fit(self._linear_func, X, Y, bounds=bounds)

        fit_slope, fit_c = popt
        fit_slope_err, fit_c_err = np.sqrt(np.diag(pcov))
        slope = uncertainties.ufloat(fit_slope, fit_slope_err)
        c = uncertainties.ufloat(fit_c, fit_c_err)
        return slope, c

    def plot_all_lin_fits(self, ax):
        for i in range(len(self.names)):
            ax.plot([0, 1], linear_func(np.array([0, 1]), self.slopes[i]), '--', label=self.names[i])
        ax.legend()

    def plot_dependency(self, ax):
        ys_with_errs = self.slopes_with_errs * self.radiuses_with_errs * self.viscosities_with_errs
        ys = [y.n for y in ys_with_errs]
        ys_errs = [y.s for y in ys_with_errs]
        ax.errorbar(self.Ts, ys, ys_errs, fmt='.',
                    label='slopes * viscosity * radius')
        ax.set_title('Slope (normalized by viscosity & radius) vs Temperature', pad=20)
        ax.set_xlabel('T [K]')
        ax.set_ylabel(r'slope*viscosity*radius [$ m^{3}\cdot Pa $]')

    def plot_expected(self, ax):
        expected_slope = self.expected_slope
        x_lim = np.array(ax.get_xlim())
        slope1 = expected_slope.n + expected_slope.s
        slope2 = expected_slope.n - expected_slope.s
        ax.plot(x_lim, linear_func(x_lim, slope1), '--r', label='expected')
        ax.plot(x_lim, linear_func(x_lim, slope2), '--r', label='expected')

    def plot_expected2(self, ax):
        x_lim = np.array(ax.get_xlim())
        expected_slope = self.expected_slope
        slope1 = expected_slope.n + expected_slope.s
        xs = np.linspace(x_lim[0], x_lim[1], 30)
        ys = linear_func(xs, expected_slope.n)
        ys_errs = linear_func(xs, slope1) - ys
        ax.errorbar(xs, ys, ys_errs)

    def plot_expected_simple(self, ax):
        xs = np.array(ax.get_xlim())
        slope = self.expected_slope.n
        ys = self._linear_func(xs, slope, 0)
        ax.plot(xs, ys, label='expected')

    def plot_fit(self, ax):
        xs = np.array(ax.get_xlim())
        ys = self._linear_func(xs, self.fit_slope.n, self.fit_c.n)
        ax.plot(xs, ys, '--', label='linear fit')

    def calc_n_sigma(self):
        diff = self.expected_slope.n - self.fit_slope.n
        err = np.sqrt(self.expected_slope.s ** 2 + self.fit_slope.s ** 2)
        return diff / err

    def _calc_expected_slope(self):
        radius = uncertainties.ufloat(1, 0.1)
        expected_slope = 2 * K / (3 * np.pi * radius)
        return expected_slope

    def print_results(self):
        print(f'expected slope: {self.expected_slope}')
        print(f'Temp dependence fit results: '
              f'slope={self.fit_slope} c={self.fit_c}')
        print(f'n sigma={self.calc_n_sigma()}')

    def print_results_latex(self):
        print(f'expected=\t\t{self.expected_slope:L}')
        print(f'fit_slope=\t\t{self.fit_slope:L}')
        print(f'fit_c=\t\t{self.fit_c:L}')
        print(r'n_{sigma}\approx ' + str(self.calc_n_sigma()))

    def analyze(self):
        self.print_results()
        fig, ax = plt.subplots()
        self.plot_dependency(ax)
        self.plot_fit(ax)
        self.plot_expected(ax)
        ax.legend()

        plt.show()


def main():
    paths = glob.glob(ROOT_FOLDER)
    analyzer = TempAnalyzer(paths)
    analyzer.analyze()


if __name__ == '__main__':
    main()

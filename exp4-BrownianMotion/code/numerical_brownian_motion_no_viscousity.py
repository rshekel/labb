import numpy as np 
import matplotlib.pyplot as plt 

plt.close('all') 


avg_t = 1
var_t = 0.4
avg_x = 0
var_x = 1

# Random times between collisions 
dts = np.random.normal(avg_t, var_t, 2000)
t = np.cumsum(dts)
t.sort()

# random velocity gained from collision 
v = np.random.normal(avg_x, var_x, 2000)

# distance traveled between collisions 
dxs = v*t

# placement 
x = np.cumsum(dxs)

plt.figure()
plt.plot(t, x, '-r')
plt.title("The walk")
plt.xlabel("Time")
plt.ylabel("x")
plt.show(block=False)

t_jumps = np.arange(1, 50)
x_means = [] 

for t_jump in t_jumps:
    xx = x[::t_jump]         # locations at t_jump intervals 
    xxx = (np.diff(xx) ** 2) # distance travelled in dtt time  - squared 
    mean_xdiff = xxx.mean()  # <x^2>
    x_means.append(mean_xdiff)

plt.figure()
plt.plot(t_jumps, x_means, '-r')
plt.title("<x^2> Vrs Delta_t")
plt.xlabel("Delta_t")
plt.ylabel("<x^2>")
plt.show(block=False)

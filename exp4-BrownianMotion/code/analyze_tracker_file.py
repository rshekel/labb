﻿import os
import re
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import uncertainties

from data import *

# 1 is method of t_jumps non overlapping
# 2 is method of t_jumps with sliding window 
OPTION = 1

K = 1.381e-23  # Boltzmann const

DATA_FOLDER = "..\\data"

def get_slope(path):
    T, X, Y = load_data(path, fix=True)
    max_jump = int(0.05 * len(T))
    Delta_t, r_means, r_errs = calc_r_sqr(T[1] - T[0], X, Y, max_jump)
    slope, slope_err = fit_to_lin(Delta_t, r_means)
    return slope, slope_err


def calc_expected_slope(temperature, radius, viscosity):
    """ in Kelvin, meter, and MKS viscosity"""
    return (2 * K * temperature) / (3 * np.pi * viscosity * radius)


class Meas(object):
    def __init__(self, path):
        self.path = path

        self.glicerol, self.radius, self.temperature = get_params_from_path(path)

        self.glicerol = calc_real_glicerol(self.glicerol)
        self.viscosity = get_viscosity_from_glicerol(self.glicerol, self.temperature)
        self.slope, self.slope_err = get_slope(path)

        self.expected_slope = calc_expected_slope(self.temperature, self.radius, self.viscosity)
        self.slope_n_sigma = self._calc_n_sigma_of_slope()

    def _calc_n_sigma_of_slope(self):
        diff = self.expected_slope.n - self.slope
        err = np.sqrt(self.expected_slope.s ** 2 + self.slope_err ** 2)
        return diff / err

    def compare_to_expected(self):
        slope = uncertainties.ufloat(self.slope, self.slope_err)
        print(f'Fit slope is {slope}')
        print(f'Expected slope is {self.expected_slope}')
        print(f'n-sigma={self.slope_n_sigma}')


def calc_r_sqr(dt, X, Y, max_jump=100):
    """ Returns Delta_Ts, r_means, r_mean_errs"""

    t_jumps = np.arange(1, max_jump)
    r_means = []
    r_errs = []

    # t_jumps is in arbitrary dt interval 
    for t_jump in t_jumps:
        # locations at t_jump intervals 
        if OPTION == 1:
            xx = X[::t_jump]
            yy = Y[::t_jump]
            xxx = (np.diff(xx) ** 2)
            yyy = (np.diff(yy) ** 2)
        elif OPTION == 2:
            xx = X[t_jump:] - X[:-t_jump]
            yy = Y[t_jump:] - Y[:-t_jump]
            xxx = xx ** 2
            yyy = yy ** 2
        else:
            raise Exception('Unknown OPTION!')

        # distances travelled in t_jump
        rrr = xxx + yyy

        # <r^2>
        mean_rdiff = rrr.mean()
        r_means.append(mean_rdiff)
        r_errs.append(rrr.std())

    # Delta_t is in seconds 
    Delta_t = t_jumps * dt

    Delta_t = np.append(0, Delta_t)
    r_means = np.append(0, r_means)
    r_errs = np.append(0, r_errs)

    return Delta_t, r_means, r_errs


def calc_r_mean(dt, X, Y, max_jump=100):
    """ Returns Delta_Ts, r_means, r_mean_errs"""

    t_jumps = np.arange(1, max_jump)
    x_means, y_means = [], []
    x_errs, y_errs = [], []

    # t_jumps is in arbitrary dt interval
    for t_jump in t_jumps:
        # locations at t_jump intervals
        if OPTION == 1:
            xx = X[::t_jump]
            yy = Y[::t_jump]
            xxx = np.diff(xx)
            yyy = np.diff(yy)
        elif OPTION == 2:
            xxx = X[t_jump:] - X[:-t_jump]
            yyy = Y[t_jump:] - Y[:-t_jump]
        else:
            raise Exception('Unknown OPTION!')

        mean_xdiff = xxx.mean()
        x_means.append(mean_xdiff)
        mean_ydiff = yyy.mean()
        y_means.append(mean_ydiff)
        x_errs.append(xxx.std())
        y_errs.append(yyy.std())

    # Delta_t is in seconds
    Delta_t = t_jumps * dt

    return Delta_t, x_means, x_errs, y_means, y_errs


def linear_func(x, m):
    return m * x


def parabool_func(x, a, b):
    return a * x ** 2 + b * x


def fit_to_lin(X, Y, from_parabool=False):
    """ returns slope, slope_err """
    if not from_parabool:
        popt, pcov = curve_fit(linear_func, X, Y)
        slope = popt[0]
        param_errs = np.sqrt(np.diag(pcov))
        slope_err = param_errs[0]
    else:
        popt, pcov = curve_fit(parabool_func, X, Y)
        slope = popt[1]
        param_errs = np.sqrt(np.diag(pcov))
        slope_err = param_errs[1]

    return slope, slope_err


def plot_walk(X, Y, save_to=None):
    plt.figure()
    plt.plot(X, Y, '-r')
    plt.plot(X[0], Y[0], '*b', label='start')
    plt.plot(X[-1], Y[-1], 'Xg', label='end')
    plt.legend()

    plt.title("Particle track in XY plane")
    plt.xlabel("X [m]")
    plt.ylabel("Y [m]")
    if save_to:
        plt.savefig(save_to)

    plt.show(block=False)


def plot_r_sqr(Delta_t, r_means, r_errs, add_lin_fit=True, save_to=None):
    fig, ax = plt.subplots()
    ax.errorbar(Delta_t, r_means, r_errs, fmt='.r', label="$ <r^{2}> $", zorder=1)

    ax.set_title(r"$ <r^{2}> $ Vrs $ \Delta t $")
    ax.set_xlabel(r"$ \Delta t $ [s]")
    ax.set_ylabel("$ <r^{2}> $ [$ m^{2} $] ")

    if add_lin_fit:
        slope, _ = fit_to_lin(Delta_t, r_means)
        expected = linear_func(Delta_t, slope)
        ax.plot(Delta_t, expected, '--', label='linear fit', zorder=2)

    ax.legend()

    if save_to:
        plt.savefig(save_to)

    plt.show(block=True)


def plot_walk_and_r_sqr(path, save_walk_to=None, save_r_sqr_to=None):
    T, X, Y = load_data(path)
    plot_walk(X, Y, save_to=save_walk_to)

    # have at least 10 measurements
    max_jump = int(0.051 * len(T))
    Delta_t, r_means, r_errs = calc_r_sqr(T[1] - T[0], X, Y, max_jump)
    slope, slope_errs = fit_to_lin(Delta_t, r_means)
    print(slope, slope_errs)
    plot_r_sqr(Delta_t, r_means, r_errs, add_lin_fit=True, save_to=save_r_sqr_to)


def plot_r_vs_t_for_water():
    path = r"..\data\week1\try5-several-sizes\mass_e_glic_0_r_25.csv"
    meas = Meas(path)
    meas.compare_to_expected()
    plot_walk_and_r_sqr(path)


def plot_r_vs_t_for_18_glicerol():
    path = r"..\data\week1\try2-several-sizes\mass_e_glic_20_r_25.csv"
    meas = Meas(path)
    meas.compare_to_expected()
    plot_walk_and_r_sqr(path)


def get_all_csvs(root_path=DATA_FOLDER):
    paths = []

    root_path = root_path or DATA_FOLDER

    if isinstance(root_path, list):
        for path in root_path:
            paths.extend(get_all_csvs(path))
        return paths

    for root, dirs, files in os.walk(root_path):
        for name in files:
            path = os.path.join(root, name)
            if path.endswith(".csv"):
                paths.append(path)
    return paths


def get_all_meass():
    meass = []
    paths = get_all_csvs()
    for path in paths:
        meass.append(Meas(path))
    return meass


def print_all_results():
    mms = get_all_meass()
    for mm in mms:
        print(
            f"radius: {mm.radius:e}, glic: {mm.glicerol},\t slope: {mm.slope:e}, expected_slope: {mm.expected_slope:e}")

    print([(mm.slope, mm.expected_slope) for mm in mms])
    print([(mm.expected_slope - mm.slope) / mm.slope_err for mm in mms])


def plot_all_results():
    paths = get_all_csvs()
    for path in paths:
        print(path)
        plot_walk_and_r_sqr(path)


def main():
    # plot_all_results()
    # plot_r_vs_t_for_water()
    # plot_r_vs_t_for_18_glicerol()
    import glob
    for path in glob.glob(r'..\data\week3\temperatures\*.csv'):
        print(path)
        # plot_walk_and_r_sqr(path)
    path = r"..\data\week1\try2-several-sizes\mass_b_glic_20_r_14.csv"
    plot_walk_and_r_sqr(path)

if __name__ == "__main__":
    plt.close('all')
    main()

import glob

import matplotlib.pyplot as plt
import numpy as np

import size_dependence
import temp_dependence
import viscosity_dependence
from analyze_tracker_file import plot_walk_and_r_sqr, K
import analyze_tracker_file
from data import load_data


def plot_walk():
    path = r"..\data\week1\try2-several-sizes\mass_b_glic_20_r_14.csv"
    path2 = r"..\graphs\walk.png"
    path3 = r"..\graphs\rsqr_lin.png"

    plot_walk_and_r_sqr(path, save_walk_to=path2, save_r_sqr_to=path3)


def calc_r_mean():
    path = r"..\data\week1\try2-several-sizes\mass_b_glic_20_r_14.csv"
    T, X, Y = load_data(path, fix=False)
    Delta_t, x_means, x_errs, y_means, y_errs = analyze_tracker_file.calc_r_mean(T[1] - T[0], X, Y)
    fig, axes = plt.subplots(2)
    axes[0].errorbar(Delta_t, x_means, x_errs, fmt='.')
    axes[1].errorbar(Delta_t, y_means, y_errs, fmt='.')

    plt.show()
    print(f'mean x: {x_means}')


def plot_size_dependence():
    paths = glob.glob(size_dependence.WATER_FOLDER)

    analyzer = size_dependence.DependenceAnalyzer(paths)

    analyzer.print_results()
    print(f'{analyzer.expected_slope:L}')
    import uncertainties
    slope = uncertainties.ufloat(analyzer.fit_slope, analyzer.fit_slope_err, 'slope')
    print(f'{slope:L}')
    c = uncertainties.ufloat(analyzer.fit_c, analyzer.fit_c_err, 'c')
    print(f'c={c:L}')

    k_boltzmann = slope * 3 * np.pi * analyzer.viscosity / (2 * analyzer.T)
    print(f'k_boltzmann\t\t{k_boltzmann:L}')
    print(f'with std errs of {k_boltzmann.std_score(K)}')

    fig, ax = plt.subplots()
    analyzer.plot_dependency(ax)
    analyzer.plot_fit(ax)
    analyzer.plot_expected_simple(ax)
    ax.legend()
    fig.savefig(r"..\graphs\size_dependence.png")


def plot_viscosity_dependence():
    analyzer = viscosity_dependence.DependenceAnalyzer(None, False)
    analyzer.print_results_latex()
    fig, ax = plt.subplots()
    analyzer.plot_dependency(ax)
    analyzer.plot_expected_simple(ax)
    print(f'base radius={analyzer.base_radius:L}')

    k_boltzmann = analyzer.fit_slope * 3 * np.pi * analyzer.base_radius / (2 * analyzer.T)
    print(f'k_boltzmann\t\t{k_boltzmann:L}')
    print(f'with std errs of {k_boltzmann.std_score(K)}')

    analyzer.plot_fit(ax)
    ax.legend()
    fig.savefig(r'..\graphs\viscosity_dependence.png')


def plot_temp_dependence():
    paths = glob.glob(temp_dependence.ROOT_FOLDER)
    analyzer = temp_dependence.TempAnalyzer(paths)
    analyzer.print_results_latex()
    fig, ax = plt.subplots()
    analyzer.plot_dependency(ax)
    analyzer.plot_expected_simple(ax)
    analyzer.plot_fit(ax)
    ax.legend()
    fig.savefig(r'..\graphs\temp_dependence.png')


def main():
    # plot_size_dependence()
    # plot_viscosity_dependence()
    # plot_temp_dependence()
    calc_r_mean()


if __name__ == '__main__':
    main()

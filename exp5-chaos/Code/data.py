import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from scipy.optimize import curve_fit
from plots import plot_all_three
from mpl_toolkits.mplot3d import Axes3D


def load_data(path, only_R=False):
    if only_R:
        data = np.loadtxt(path, usecols=(3, 4), delimiter=',')
        ts, R_volts = data.transpose()
        return ts, R_volts, None, None

    data = np.loadtxt(path, usecols=(3, 4, 10, 16), delimiter=',')
    ts, R_volts, diode_volts, ext_volts = data.transpose()
    diode_volts = diode_volts - R_volts
    return ts, R_volts, diode_volts, ext_volts


def sin_func(x, a, w, c, f):
    return a * np.sin(w * x + f) + c


def fit_am_sin(ts, ext_volts, plot=True):
    peaks, _ = sig.find_peaks(ext_volts, width=30)
    vals, pcov = curve_fit(sin_func, ts[peaks], ext_volts[peaks])
    if plot:
        plt.figure()
        plt.plot(ts, ext_volts, '.')
        plt.plot(ts[peaks], ext_volts[peaks], '*g')
        plt.plot(ts, sin_func(ts, *vals))
        plt.show()
    return vals


def play_plot_three():
    path = r'..\Data\week2\am-0-5V.csv'
    ts, R_volts, diode_volts, ext_volts = load_data(path)
    plot_all_three(ts, R_volts, diode_volts, ext_volts)


def play_am_bifurcation(use_diode=True):
    path = r'..\Data\week3\am-0-6V.csv'
    # path = r'..\Data\week3\am-0-10V.csv'

    # path = r'..\Data\week2\am-0-5V.csv'
    # path = r'..\Data\week2\am-0-5V-highres.csv' # need to crop some out of here, or maybe other problem? 
    # path = r'..\Data\week2\am-0-5V-highres-2.csv'
    ts, R_volts, diode_volts, ext_volts = load_data(path)

    only_upslope = True    
    if only_upslope:
        max_index = np.argmax(ext_volts)
        ts = ts[:max_index]
        R_volts = R_volts[:max_index]
        diode_volts = diode_volts[:max_index]
        ext_volts = ext_volts[:max_index]

    sin_args = fit_am_sin(ts, ext_volts)

    volts = diode_volts
    if not use_diode:
        volts = R_volts

    should_sparse_data = False
    if should_sparse_data:
        group_size = 5
        volts = sparse_data_by_averaging(volts, group_size)
        ts = ts[::group_size]

    peaks, _ = sig.find_peaks(volts, prominence=(0.05, None))

    if not use_diode:
        peaks, _ = sig.find_peaks(volts, prominence=(0.0009, None), width=5)
    
    v_peak = volts[peaks]
    v_ext_at_peak = sin_func(ts[peaks], *sin_args)

    if True:
        plt.figure()
        plt.plot(ts, volts, "-")
        plt.plot(ts[peaks], volts[peaks], '*g')

    plt.figure()
    plt.plot(v_ext_at_peak, v_peak, ".")
    plt.title("v_peak Vrs v_ext_at_peak")
    plt.xlabel("v_ext_at_peak")
    plt.ylabel("v_peak")
    plt.show()


def sparse_data_by_averaging(volts, group_size):
    # Cut edges to make size divisible by group_size
    crop = len(volts) % group_size
    volts = volts[crop:]
    volts = volts.reshape(-1, group_size).mean(axis=1)
    return volts


def play_return_map():
    path = r'..\Data\week2\am-0-5V-highres.csv'
    path = r'..\Data\week2\am-0-5V.csv'
    path = r'..\Data\week3\ret-map3.3V.csv'
    path = r'..\Data\week3\ret-map4V.csv'   
    path = r'..\Data\week3\ret-map3.3V.csv'
    ts, R_volts, diode_volts, ext_volts = load_data(path)

    chaos_filter = False

    volts = -R_volts

    if chaos_filter:
        # Use only data that behave chaotically
        chaos_indexes = np.where(ext_volts > 4)[0]
        first = chaos_indexes[0]
        last = chaos_indexes[-1]
        volts = volts[first:last]
        ts = ts[first:last]

    peaks, _ = sig.find_peaks(volts, prominence=(0.005, None))
    v_peak = volts[peaks]

    if True:
        plt.figure()
        plt.plot(ts, volts, "-")
        plt.plot(ts[peaks], volts[peaks], '*g')
        plt.show()

    plt.figure()
    plt.plot(v_peak[:-1], v_peak[1:], ".")
    plt.title("return map")
    plt.xlabel("v_peak")
    plt.ylabel("v_peak + 1")

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(v_peak[:-2], v_peak[1:-1], v_peak[2:], ".")
    ax.set_title("return map - 3D")
    ax.set_xlabel("v_peak")
    ax.set_ylabel("v_peak + 1")
    ax.set_zlabel("v_peak + 2")

    plt.show()


def play_density():
    # With caos - fills space
    path = r'..\Data\week3\ret-map3V.csv'
    ts, R_volts, diode_volts, ext_volts = load_data(path)
    plt.figure()
    plt.plot(ext_volts, diode_volts, '.')
    plt.title("3V - chaos fills space")
    plt.xlabel("ext_volts")
    plt.ylabel("diode_volts")
    plt.show()

    # Without caos - does not fill space
    path = r'..\Data\week3\ret-map0.5V.csv'
    ts, R_volts, diode_volts, ext_volts = load_data(path)
    plt.figure()
    plt.plot(ext_volts, diode_volts, '.')
    plt.title("0.5V - not chaos - not fills space")
    plt.xlabel("ext_volts")
    plt.ylabel("diode_volts")
    plt.show()



if __name__ == "__main__":
    play_am_bifurcation(False)
    # play_return_map()
    # play_density()
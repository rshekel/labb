import matplotlib.axes
import matplotlib.pyplot as plt
import numpy as np

import fitter


class Simulator(object):
    def __init__(self, map_func, xlim):
        self.map_func = map_func
        self.xlim = xlim

    def plot_map(self, ax):
        xs = np.linspace(self.xlim[0], self.xlim[1], 200)
        ax.plot(xs, self.map_func(xs))

    def plot_cobweb(self, ax, x0, ns):
        self.plot_map(ax)
        x = x0
        y = 0
        for i in range(ns):
            new_y = self.map_func(x)
            ax.plot((x, x), (y, new_y))
            new_x = new_y
            ax.plot((x, new_x), (new_y, new_y))
            x = new_x
            y = new_y

        ax.plot((self.xlim[0], self.xlim[1]), (self.xlim[0], self.xlim[1]), '--b')

    def plot_iterations(self, ax, x0, ns):
        x = x0
        y = self.map_func(x)
        for i in range(ns):
            ax.plot(i, y, '.')
            x = y
            y = self.map_func(x)

    def calc_path(self, x0, ns):
        ys = np.zeros(ns)
        ys[0] = x0
        for i in range(1, ns):
            x = ys[i - 1]
            ys[i] = self.map_func(x)
        return ys


class LyapunovExponentAnalyser(object):
    def __init__(self, simulator: Simulator, x0, epsilon, steps):
        self.simulator = simulator
        self.x0 = x0
        self.epsilon = epsilon
        self.steps = steps

        self.ns = np.arange(steps)
        self.path1 = simulator.calc_path(x0, steps)
        self.path2 = simulator.calc_path(x0 + epsilon, steps)
        self.diff = np.abs(self.path2 - self.path1)

        exp_func = lambda x, a: epsilon * np.exp(a * x)
        params = fitter.FitParams(exp_func, self.ns, self.diff)
        self.fit = fitter.FuncFit(params)

    def plot_paths(self, ax):
        ax.plot(self.ns, self.path1, '.')
        ax.plot(self.ns, self.path2, '.')

    def plot_diff(self, ax: matplotlib.axes.Axes):
        ax.plot(self.ns, self.diff, '.', label='difference')
        ax.set_xlabel('step')
        ax.set_ylabel('[V]')
        ax.set_title('Difference between 2 trajectories')

    def plot_diff_fit(self, ax: matplotlib.axes.Axes):
        self.fit.plot_data(ax, 'fit to exponent')


def return_map1(x):
    # Adding "noise" by  + 0.001, otherwise, solutions reach 0 and stay there
    return (-2 * x + 0.001) % 1


def return_map_triangle(x):
    return 1 - x


def main():
    simulator = Simulator(return_map_triangle, (0, 1))

    fig, ax = plt.subplots()
    simulator.plot_map(ax)

    fig, ax = plt.subplots()
    simulator.plot_cobweb(ax, 0.7, 1000)

    fig, ax = plt.subplots()
    simulator.plot_iterations(ax, 0.2233, 90)
    simulator.plot_iterations(ax, 0.2234, 90)
    plt.show()


if __name__ == '__main__':
    main()

import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import itertools


class LogisticMap(object):
    LARGE_N = 300
    SIMULATIONS_PER_R = 200
    RS_COUNT = 150

    def plot_bifurcation_diagram(self, ax=None):
        if ax is None:
            _, ax = plt.subplots()

        rs = np.linspace(0, 4, self.RS_COUNT)
        colors_iter = iter(itertools.cycle(matplotlib.rcParams['axes.prop_cycle']))
        color = None
        steady_states_count = 0
        for r in rs:
            steady_states = self.calc_steady_states(r)
            if steady_states_count < len(steady_states):
                color = next(colors_iter)['color']
                steady_states_count = len(steady_states)
            ax.plot([r] * len(steady_states), steady_states, '.', color=color)

    def calc_steady_states(self, r):
        x0s = np.random.rand(self.SIMULATIONS_PER_R)
        sim = Simulation(r)
        steady_x = np.array([sim.calc_x(x0, self.LARGE_N) for x0 in x0s])
        return np.unique(steady_x.round(decimals=5))


class Simulation(object):
    def __init__(self, r):
        self.r = r

    def calc_x(self, x0, n):
        x = x0
        for i in range(n):
            x = self.calc_next(x)
        return x

    def calc_next(self, x_n):
        return self.r * x_n * (1 - x_n)


def main():
    logistic_map = LogisticMap()
    logistic_map.plot_bifurcation_diagram()
    plt.show()


if __name__ == '__main__':
    main()

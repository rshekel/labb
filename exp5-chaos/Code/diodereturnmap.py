import re

import numpy as np
import scipy.signal as sig
from matplotlib import pyplot as plt
from uncertainties import unumpy

import returnmapsimulator
from data import load_data
from fitter import FuncFit, FitParams


class DiodeReturnMap(object):
    def __init__(self, path):
        ts, R_volts, diode_volts, ext_volts = load_data(path, only_R=True)

        self.ts = ts
        volts = -R_volts
        self.volts = volts

        self.peaks, _ = sig.find_peaks(volts, prominence=(0.005, None))
        self.v_peak = volts[self.peaks]
        self.X, self.Y = self.v_peak[:-1], self.v_peak[1:]
        self.fit_results: FuncFit = None

    def plot_V_vs_time(self, ax):
        ax.plot(self.ts, self.volts, "-")
        ax.plot(self.ts[self.peaks], self.v_peak, '*g')

    def plot_return_map_2d(self, ax):
        lines = ax.plot(self.X, self.Y, ".")
        return lines[0]

    def plot_return_map_on_z_plane(self, ax, z):
        lines = ax.plot(z*np.ones(len(self.X)), self.X, self.Y, ".")
        return lines[0]


    def fit(self, fit_func, volt_limit=None):
        X, Y = self.X, self.Y
        if volt_limit is not None:
            lower, upper = volt_limit
            relevant = np.logical_and(lower <= X, X <= upper)
            X = X[relevant]
            Y = Y[relevant]
        params = FitParams(fit_func, X, Y)
        self.fit_results = FuncFit(params)

    def plot_fit(self, ax):
        self.fit_results.plot_fit(ax, 'polynomial fit')

    def remove_peaks_below_fit(self, volt_limit=None):
        X, Y = self.X, self.Y
        if volt_limit is None:
            volt_limit = (-np.inf, np.inf)
        lower, upper = volt_limit
        relevant = np.logical_and(lower <= X, X <= upper)

        below_indexes = Y < self.fit_results.eval_func(X)
        indexes_to_remove = np.logical_and(relevant, below_indexes)
        good_indexes = np.logical_not(indexes_to_remove)
        self.X = X[good_indexes]
        self.Y = Y[good_indexes]

    @staticmethod
    def add_return_map_text(ax):
        ax.set_title("return map")
        ax.set_xlabel("v_peak")
        ax.set_ylabel("v_peak + 1")


def plot_all_return_maps(paths):
    fig, ax = plt.subplots()

    for path in paths:
        ret_map = DiodeReturnMap(path)
        ext_volt = re.match(r'.*ret-map(.*)\.csv', path).group(1)
        line = ret_map.plot_return_map_2d(ax)
        line.set_label(ext_volt)

    DiodeReturnMap.add_return_map_text(ax)
    ax.legend()
    plt.show()


def plot_ret_map_vs_v():

    import glob
    paths_week4 = glob.glob(r'..\Data\week4\ret*.csv')
    paths = paths_week4

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    for path in paths:
        ret_map = DiodeReturnMap(path)
        ext_volt = re.match(r'.*ret-map(.*)\.csv', path).group(1)
        v = float(ext_volt[:-1])
        line = ret_map.plot_return_map_on_z_plane(ax, v)
        line.set_label(ext_volt)

    plt.show()

def plot_all():
    all_paths = [
        r'..\Data\week3\ret-map3.3V.csv',
        r'..\Data\week3\ret-map3.4V.csv',
        r'..\Data\week3\ret-map3.6V.csv',
        r'..\Data\week3\ret-map3.8V.csv',
        r'..\Data\week3\ret-map3.9V.csv',
        r'..\Data\week3\ret-map3.95V.csv',
        r'..\Data\week3\ret-map4V.csv', ]

    paths = [
        r'..\Data\week3\ret-map3.6V.csv',
        r'..\Data\week3\ret-map3.8V.csv',
        r'..\Data\week3\ret-map3.9V.csv',
        r'..\Data\week3\ret-map3.95V.csv',
        r'..\Data\week3\ret-map4V.csv', ]

    import glob
    paths_week4 = glob.glob(r'..\Data\week4\ret*.csv')
    paths = paths_week4
    paths = paths[:6]
    plot_all_return_maps(paths)


def find_fit1():
    path = r'..\Data\week3\ret-map3.9V.csv'
    fig, ax = plt.subplots()

    ret_map = DiodeReturnMap(path)
    ret_map.plot_return_map_2d(ax)
    ret_map.fit(poly5_fit_func, (-np.inf, 0.032))
    ret_map.plot_fit(ax)

    ret_map.remove_peaks_below_fit((-np.inf, 0.032))
    ret_map.fit(poly5_fit_func, (-np.inf, 0.035))
    ret_map.fit_results.plot_data(ax)
    ret_map.plot_fit(ax)

    print(unumpy.nominal_values(ret_map.fit_results.fit_results))
    ret_map.fit_results.print_results()

    plt.show()

    """Fit results:
    fit a=-0.11+/-0.24
    fit b=(4+/-5)e+01
    fit c=(-3+/-4)e+03
    fit d=(1.3+/-1.4)e+05
    fit e=(-2.7+/-2.6)e+06
    fit f=(2.2+/-2.0)e+07
    """


def find_fit2():
    path = r'..\Data\week3\ret-map3.9V.csv'
    fig, ax = plt.subplots()

    ret_map = DiodeReturnMap(path)
    ret_map.plot_return_map_2d(ax)
    ret_map.fit(linear_fit_func, (0.035, np.inf))
    ret_map.plot_fit(ax)

    print(unumpy.nominal_values(ret_map.fit_results.fit_results))
    ret_map.fit_results.print_results()

    plt.show()


class DiodeReturnMapSimulator(object):
    def __init__(self, shift=0.0):
        self.x_shift = shift
        self.y_x_shift_ratio = 0.7757475  # like the linear part
        return_map_func = np.vectorize(self._shifted_return_map_func)
        x_lim = (0.015 + shift, 0.06 + shift)
        self.simulator = returnmapsimulator.Simulator(return_map_func, x_lim)

    def _shifted_return_map_func(self, x):
        new_x = x - self.x_shift
        y = self._base_return_map_func(new_x)
        new_y = y + self.x_shift * self.y_x_shift_ratio
        return new_y

    def _base_return_map_func(self, x):
        if x < 0.035:
            a, b, c, d, e, f = -1.14829811e-01, 3.79275977e+01, - 3.21470684e+03, \
                               1.32281633e+05, - 2.72399316e+06, 2.20680064e+07
            y = poly5_fit_func(x, a, b, c, d, e, f)
        else:
            a, b = -0.00922046, 0.7757475
            a -= 0.0002
            y = linear_fit_func(x, a, b)
        return y


def simulate_return_map():
    simulator = DiodeReturnMapSimulator()

    fig, ax = plt.subplots()
    simulator.simulator.plot_map(ax)

    simulator = DiodeReturnMapSimulator(shift=-0.025)
    # simulator.simulator.plot_cobweb(ax, 0.02, 500)
    simulator.simulator.plot_cobweb(ax, 0.03, 500)
    simulator.simulator.plot_map(ax)

    path = r'..\Data\week3\ret-map3.9V.csv'
    ret_map = DiodeReturnMap(path)
    ret_map.plot_return_map_2d(ax)

    plt.show()


def poly5_fit_func(x, a, b, c, d, e, f):
    return a + b * x + c * x ** 2 + d * x ** 3 + e * x ** 4 + f * x ** 5



def poly9_fit_func(x, a, b, c, d, e, f, g, h, i, j):
    return a + b * x + c * x ** 2 + d * x ** 3 + e * x ** 4 + f * x ** 5 + \
           g * x ** 6 + h * x ** 7 + i * x ** 8 + j * x ** 9


def linear_fit_func(x, a, b):
    return a + b * x


def main():
    # plot_ret_map_vs_v()
    # plot_all()
    # find_fit1()
    # find_fit2()
    simulate_return_map()


if __name__ == '__main__':
    main()

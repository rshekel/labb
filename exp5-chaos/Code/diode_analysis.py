import matplotlib.pyplot as plt
import numpy as np

from fitter import FitParams, FuncFit


class DiodeAnalyzer(object):
    def __init__(self, data_file, crop_diode_volts=None, crop_R_volts=None):
        data = np.loadtxt(data_file, usecols=(3, 4, 10), delimiter=',')
        self.ts, self.R_volts, self.diode_volts = data.transpose()

        self.R_volts = -self.R_volts

        self._crop_values(crop_diode_volts, crop_R_volts)
        # self._sparse_data_by_averaging(group_size=1)

        fit_params = FitParams(self._exp_func, self.diode_volts, self.R_volts)
        # Bounds of     x0, m, b
        upper_bounds = [10, 5, 1000]
        lower_bounds = [-10, 0, -1000]
        fit_params.bounds = (lower_bounds, upper_bounds)
        self.fit = FuncFit(fit_params)
        self.fit_x0, self.fit_m, self.fit_b = self.fit.fit_results
        self.diode_n = self._calc_diode_n()

    def _exp_func(self, x, x0, m, b):
        return b * (np.exp(m * (x - x0)) - 1)

    def _crop_values(self, crop_diode_volts, crop_R_volts):
        if crop_R_volts:
            indexes = np.logical_and(crop_R_volts[0] < self.R_volts, self.R_volts < crop_R_volts[1])
            self.R_volts = self.R_volts[indexes]
            self.diode_volts = self.diode_volts[indexes]

        if crop_diode_volts:
            indexes = np.logical_and(crop_diode_volts[0] < self.diode_volts, self.diode_volts < crop_diode_volts[1])
            self.R_volts = self.R_volts[indexes]
            self.diode_volts = self.diode_volts[indexes]

    def _sparse_data_by_averaging(self, group_size):
        # Cut edges to make size divisible by group_size
        crop = len(self.diode_volts) % group_size
        self.diode_volts = self.diode_volts[crop:]
        self.R_volts = self.R_volts[crop:]
        self.diode_volts = self.diode_volts.reshape(-1, group_size).mean(axis=1)
        self.R_volts = self.R_volts.reshape(-1, group_size).mean(axis=1)

    def _calc_diode_n(self):
        q_e = 1.602e-19
        k_B = 1.381e-23
        T = 300

        n = q_e / (self.fit_m * k_B * T)
        return n

    def plot_all(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()

        self.fit.plot_data(ax)
        self.fit.plot_fit(ax)
        ax.set_xlabel('Diode Voltage [V]')
        ax.set_ylabel('Resistor Voltage [V]')
        self.fit.print_results()
        print(f'diode n={self.diode_n}')


def plot_raw():
    path = r'..\Data\week1\V_V3.csv'
    data = np.loadtxt(path, usecols=(3, 4, 10), delimiter=',')
    ts, R_volts, diode_volts = data.transpose()
    plt.figure()
    plt.plot(ts, R_volts, label="Resistor volts")
    plt.plot(ts, diode_volts, label="diode volts")
    plt.xlabel('time [s]')
    plt.ylabel('Voltage [V]')

    plt.legend()
    plt.show()


def plot_exp_fit():
    diode_analyzer = DiodeAnalyzer(r'..\Data\week1\V_V3.csv', (-np.inf, -0.08), (-np.inf, np.inf))
    # diode_analyzer = DiodeAnalyzer(r'..\Data\week1\V_V3.csv')
    diode_analyzer.plot_all()
    plt.show()


if __name__ == '__main__':
    plot_exp_fit()

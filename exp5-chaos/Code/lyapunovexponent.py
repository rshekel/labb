import matplotlib.axes
import matplotlib.pyplot as plt
import numpy as np

import diodereturnmap
import returnmapsimulator


def plot_divergence():

    x0 = 0.024
    epsilon = 1e-8
    steps = 50
    diode_retmap = diodereturnmap.DiodeReturnMapSimulator()
    simulator = diode_retmap.simulator
    lyapunov = returnmapsimulator.LyapunovExponentAnalyser(simulator, x0, epsilon, steps)

    fig, ax = plt.subplots()
    ax: matplotlib.axes.Axes = ax
    lyapunov.plot_diff(ax)
    lyapunov.fit.plot_fit(ax, 'fit to exponent')

    ax.set_title("$ |V_{n}-V_{n}'| $ vs n (steps)")
    ax.set_xlabel("n steps")
    ax.set_ylabel("difference [V]")

    ax.legend()

    lyapunov.fit.print_results()

    plt.show()


def calc_several_lyapunov():
    epsilon = 1e-8
    steps = 20
    x0s = np.linspace(0.02, 0.05, 15)

    diode_retmap = diodereturnmap.DiodeReturnMapSimulator()
    simulator = diode_retmap.simulator
    fig, axes = plt.subplots(5, 3)
    for i, x0 in enumerate(x0s):
        lyapunov = returnmapsimulator.LyapunovExponentAnalyser(simulator, x0, epsilon, steps)
        lyapunov.fit.print_results()
        lyapunov.plot_diff(axes.flat[i])

    # plt.show()


def main():
    # plot_divergence()
    calc_several_lyapunov()


if __name__ == '__main__':
    main()

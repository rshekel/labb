import numpy as np 
import matplotlib.pyplot as plt 

def plot_all_three(ts, R_volts, diode_volts, ext_volts):
    plt.figure()
    plt.plot(ts, R_volts, ".", label="R_volts")
    plt.plot(ts, diode_volts, ".", label="diode_volts")
    plt.plot(ts, ext_volts, ".", label="ext_volts")
    plt.legend()
    plt.show()


import re
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from utils import print_params


class SimpleV2Rad(object):

    def __init__(self, offset, rad_per_v):
        self._offset = offset
        self._rad_per_v = rad_per_v
        
    def convert(self, volts):
        return self._offset + self._rad_per_v * volts


class V2Rad(object):

    TIME_ON_STEP = 3  # In seconds
    STEP_SIZE_DEGREES = 2.5
    FIRST_ANGLE_DEGREES = -15
    V_EPSILON = 0.1
    SHOULD_PLOT = False  # For debugging

    def __init__(self, calibration_path):
        self._offset = 0
        self._rad_per_v = 0
        self.caliberate(calibration_path)

    def convert(self, volts):
        return self._offset + self._rad_per_v * volts

    def caliberate(self, path):
        volts, ts, sample_rate = self._load_data(path)
        
        if self.SHOULD_PLOT:
            fig, ax = plt.subplots()
            ax.plot(ts, volts)
            fig.show()
        
        
        """
        xx = 60 + 167 * np.arange(0, 12)
        xx_volts = volts[xx]
        yy_degrees = self.FIRST_ANGLE_DEGREES + self.STEP_SIZE_DEGREES * np.arange(0, 12)
        """
        xx = int(12*sample_rate) + int(10*sample_rate) * np.arange(0, 15)
        xx_volts = volts[xx]
        yy_degrees = -15+2 + 2 * np.arange(0, 15)
        
        yy_rads = yy_degrees / 180 * np.pi
        linear_func = lambda v, a, b: a*v+b
        
        
        popt, pcov = curve_fit(linear_func, xx_volts, yy_rads)
        
        params_errors = np.sqrt(np.diag(pcov))
        print_params(["a", "b"], popt, params_errors)

        self._offset = popt[1]
        self._rad_per_v = popt[0]
        
        if self.SHOULD_PLOT:    
            # Ugly, but it works, it finds the flat lines indicating a step
            ax.plot(ts[xx], volts[xx], "*")
            
            fig, ax = plt.subplots()
            ax.set_title("Rads vs Volts")
            ax.plot(xx_volts, yy_rads, '*')
            xx = np.arange(-10,-1, 0.02)
            yy = linear_func(xx, *popt)
            ax.plot(xx, yy, label="Linear fit")
            

    def _load_data(self, path):
        data = pd.read_table(path, header=6, sep='\t')

        volts = data.get("Ch1[V]")
        ts = data.get("Time[s]")

        # Find smaple rate:
        with open(path, 'r') as f:
            headers = f.read(1000)
        m = re.findall(r"Sample Rate \(Hz\): (\d+)", headers)
        sample_rate = int(m[0])

        return volts, ts, sample_rate

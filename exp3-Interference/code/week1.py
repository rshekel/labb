import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import v2rad
from InterferenceFit import InterferenceFit, DoubleSlitInterferenceFit

WEEK1_CALIBRATION_PATH = "../results/week1/v_vs_t_for_calibration.txt"
WEEK2_CALIBRATION_PATH = "../results/week2/calibration.txt"
LASER_WL = 632.8e-9
LASER_K = 2*np.pi/LASER_WL
 

def get_vtor(calibration_path=None):
    if not calibration_path:
        calibration_path = WEEK2_CALIBRATION_PATH

    vtor = v2rad.V2Rad(calibration_path)
    # vtor = v2rad.V2Rad(0.0041500, 0.0914919)
    return vtor


def single_slit(i=1):
    print("Vtor")
    vtor = get_vtor()
    
    print("Slit")    
    if i == 1:
        slit_width = 0.04e-3
        ifit = InterferenceFit(vtor, "../results/week1/1slit_typeB.txt")
    elif i == 2:
        slit_width = 0.08e-3
        ifit = InterferenceFit(vtor, "../results/week1/1slit_typeC.txt")        
    elif i == 3:
        slit_width = 0.02e-3
        ifit = InterferenceFit(vtor, "../results/week2/Slit1_A.txt")
    elif i == 4:
        slit_width = 0.16e-3
        ifit = InterferenceFit(vtor, "../results/week2/Slit1_D1.txt")
    elif i == 5:
        slit_width = 0.16e-3
        ifit = InterferenceFit(vtor, "../results/week2/Slit1_D2.txt")

    ifit.print_fit_params()
    ifit.plot(slit_width=slit_width)
    
    print("Expected kb: {}".format(slit_width*LASER_K))
    ifit.residuals()


def double_slit(i=1):
    print("Vtor")
    vtor = get_vtor()
    
    print("Slit")    
    if i == 1:
        slit_width = 0.04e-3
        slits_dist = 0.25e-3
        ifit = DoubleSlitInterferenceFit(vtor, "../results/week2/slit2_A1.txt")
        
    elif i == 2:
        slit_width = 0.04e-3
        slits_dist = 0.25e-3
        ifit = DoubleSlitInterferenceFit(vtor, "../results/week2/slit2-A2-slitondetector.txt")

    elif i == 3:
        slit_width = 0.04e-3
        slits_dist = 0.25e-3
        ifit = DoubleSlitInterferenceFit(vtor, "../results/week2/slit2-A2-bighole.txt")
        
        
    # ifit.print_fit_params()
    # ifit.plot()

    print("Expected kb: {}".format(slit_width*LASER_K))
    print("Expected ka: {}".format(slits_dist*LASER_K))

    # ifit.residuals()


if __name__ == "__main__":
    plt.close("all")
    mpl.rcParams['font.size'] = 16
    
    # single_slit(5)
    double_slit(2)
    # v = get_vtor(WEEK2_CALIBRATION_PATH)

import numpy as np 
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from utils import print_params
from uncertainties import ufloat


class Fit(object):
    
    _theoretical_power_shape = "parabool"
    _fit_bounds = ((0, -0.001, -5), (1e4, 0.001, 5))
    
    def __init__(self, path, quiet=False):
        self.path = path 
        self.load_data()
        
        if not quiet:
            self.calc_fit_params()
            self.plot_fit()
        
    def load_data(self):
        data = np.loadtxt(path, skiprows=1)
        degrees = data[:, 1]
        # Not fixing to zero - because we don't know the exact zero until after the fit to parabool
        self.alpha = np.radians(degrees)
        # TODO: do this automatically 
        self.alpha = self.alpha - 3.156
        self.m = data[:, 0]

        
    def parabool(self, x, a):#, b, c):
        # assuming ax^2 + bx + c
        return a*x**2#  +b*x + c
            
    def calc_fit_params(self, set_params=True):        
        # popt, pcov = curve_fit(self.parabool, self.alpha, self.m, bounds=self._fit_bounds)
        popt, pcov = curve_fit(self.parabool, self.alpha, self.m)
        
        if set_params:
            self.param_errs = np.sqrt(np.diag(pcov))
            self._popt = popt
        
        return popt, pcov         
        
    @property
    def a(self):
        return self._popt[0]

    @property
    def b(self):
        return self._popt[1]

    @property
    def c(self):
        return self._popt[2]
        
    def print_fit_params(self):
        print_params(["a"], [self.a], self.param_errs)
        
    def plot_raw(self):
        fig, ax = plt.subplots()
        ax.set_title(r"$ \Delta m $ vs $ \Delta\alpha $ ", fontdict={'fontsize': 18})
        ax.set_xlabel(r"$ \Delta\alpha $ [Rads]", fontdict={'fontsize': 16})
        ax.set_ylabel(r"$ \Delta m $ [Int]", fontdict={'fontsize': 16})
        ax.plot(self.alpha, self.m, '.-', label="Measured data", )
        return fig, ax 
        
    def plot_fit(self):
        _, ax = self.plot_raw()
        
        start = np.min(self.alpha)
        end = np.max(self.alpha)
        dist = end - start
        xx = np.arange(start-0.1*dist, end+0.1*dist, 0.001)
        yy = self.parabool(xx, *self._popt)
        ax.plot(xx, yy, '--', label=f"Fit to {self._theoretical_power_shape}")
        ax.legend()
    
    def residuals(self):
        plt.figure("Residuals")
        
        theory_m = self.parabool(self.alpha, *self._popt)
        difference = theory_m - self.m
        plt.plot(self.alpha, difference,'or')
        plt.grid()
        plt.title("Residuals")
    

class NVSAirPressure(object):
    
    _theoretical_power_shape = "linear"
    
    def __init__(self, path, quiet=False):
        self.path = path 
        self.load_data()
        
        if not quiet:
            self.calc_fit_params()
            self.plot_fit()
        
    def load_data(self):
        data = np.loadtxt(path, skiprows=1)
        self.pressure_bars = data[:, 1]
        self.m = data[:, 0]
        
    def linear(self, x, m, c):
        return m*x+c
            
    def calc_fit_params(self, set_params=True):        
        popt, pcov = curve_fit(self.linear, self.pressure_bars, self.m)
        
        if set_params:
            self.param_errs = np.sqrt(np.diag(pcov))
            self._popt = popt
        
        return popt, pcov         
        
    @property
    def a(self):
        return self._popt[0]

    @property
    def c(self):
        return self._popt[1]

    def print_fit_params(self):
        print_params(["a", "c"], [self.a, self.c], self.param_errs)
        
    def plot_raw(self):
        fig, ax = plt.subplots()
        ax.set_title(r"$ \Delta m $ vs Pressure", fontdict={'fontsize': 18})
        ax.set_xlabel(r"Pressure [bar]", fontdict={'fontsize': 16})
        ax.set_ylabel(r"$ \Delta m $ [Int]", fontdict={'fontsize': 16})
        ax.plot(self.pressure_bars, self.m, '.-', label="Measured data")
        return fig, ax 
        
    def plot_fit(self):
        _, ax = self.plot_raw()
        
        start = np.min(self.pressure_bars)
        end = np.max(self.pressure_bars)
        dist = end - start
        xx = np.linspace(start-0.1*dist, end+0.1*dist, 1000)
        yy = self.linear(xx, *self._popt)
        ax.plot(xx, yy, '--', label=f"Fit to {self._theoretical_power_shape}")
        ax.legend()
    
    def residuals(self):
        plt.figure("Residuals")
        
        start = np.min(self.pressure_bars)
        end = np.max(self.pressure_bars)
        dist = end - start
        
        theory_m = self.linear(self.pressure_bars, *self._popt)
        difference = theory_m - self.m
        plt.plot(self.pressure_bars, difference,'or')
        plt.plot([start-0.1*dist, end+0.1*dist], [0, 0], '--')
        plt.grid()
        plt.title("Residuals")
    
    def extract_n0(self):
        L = 6.55e-2
        L_err = 0.5e-2
        P0_bars = 1013
        #LASER_WL = 633e-9

        extracted_a = ufloat(self.a, self.param_errs[0])
        L_ufloat = ufloat(L, L_err)

        measured_n0 = -extracted_a * LASER_WL/2 * P0_bars / L_ufloat + 1
        print(f"Measured n0={measured_n0}")
        measured_n0 = -self.a * LASER_WL/2 * P0_bars / L + 1
        measured_n0_err = self.param_errs[0] * LASER_WL/2 * P0_bars / L
        print(f"Measured n0={measured_n0}+-{measured_n0_err}")
        

LASER_WL = 632.8e-9
GLASS_THICKNESS = 1e-3
    
if __name__ == "__main__":
    plt.close('all')
    
    glass = False
    if glass:
        path = r"..\results\week3\glass_alpha_m_2.txt"
        f = Fit(path)
        f.print_fit_params()
        f.residuals()
        a = f.a
        y = 1e-3
        n = 1 / (1 - (a*LASER_WL)/GLASS_THICKNESS)
        print("n: ", n)
        

    air = True
    if air:    
        path = r"..\results\week3\pressure_bar_m.txt"
        f = NVSAirPressure(path)
        f.print_fit_params()
        f.residuals()
        f.extract_n0()

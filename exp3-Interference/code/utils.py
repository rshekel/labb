# -*- coding: utf-8 -*-
"""
Created on Tue Dec 31 22:04:35 2019

@author: Alon
"""

def print_params(names, values, errors, rss=False):
    for name, value, error in zip(names, values, errors):
        if not rss:
            print("{}={:+.7f}~{:.7f}".format(name, value, error))
        else:
            err_percent = (error / value) * 100
            print("{}={:+.4f}+-{:.1f}%".format(name, value, err_percent))
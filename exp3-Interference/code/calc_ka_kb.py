# coding: utf-8
from math import *
import numpy as np

# distance (in theta rads) between peaks
peaks_t = [0.0083, 0.0052, 0.0023, 0.0, -0.0021, -0.0035, -0.0055, -0.008]
diffs = -np.diff(peaks_t)
print(diffs)
T = np.mean(diffs)
dT = np.std(diffs)

bs = [0.0246, 0.0274, 0.0303, 0.0338]
diffs_b = -np.diff(bs)
diffs_b

# ka/2 = omega*t;
# for cos^2 omega=pi/peak_T
ka = 2*pi/T
z = 2*pi/T
dz = z*dT/T

print(f"T={z}+-{dz}")
K_LASER = 2*pi/(632.8e-9)
slits_dist = 0.25e-3
print(f"expected {slits_dist*K_LASER}")


sinc_first_zero = 0.017
sinc_zero_to_zero_large = 0.0229-(-0.0199)
sinc_zero_to_zero_small = 0.016-(-0.014)
print("Smallest kb:")
print(2*pi/(sinc_zero_to_zero_large/2))

print("Largest kb:")
print(2*pi/(sinc_zero_to_zero_small/2))

print("another kb:")
print(2*pi/sinc_first_zero)
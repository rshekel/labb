import numpy as np
import pandas as pd
import matplotlib as mpl

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from utils import print_params


class InterferenceFit(object):
    
    _theoretical_power_shape = "sinc"
    _fit_bounds = ((0, -0.001), (1e4, 0.001))
    
    def __init__(self, vtor, path):
        data = pd.read_table(path, header=6, sep='\t')
        self._vtor = vtor
        self.volts = data.get("Ch1[V]")
        self.thetas = vtor.convert(self.volts)
        
        self.power = data.get("Ch0[V]")
        self.power *= -1  # For some reason, it is upside down
        
        index_max = self.power.idxmax()
        self.norm_power = self.power / self.power[index_max]
        self.norm_thetas = self.thetas - self.thetas[index_max]
        
        self._fit()
        # self._plot_raw()

    def _calc_power(self, t, kb, c):
        # Assuming power is normalized and theta is centered
        beta = kb/2*np.sin(t-c)
        
        power = (np.sin(beta) / beta) ** 2
        
        # Value of sin(x)/x is 1 when x->0 and not inf (or NaN)
        should_fix_0_values = True
        if should_fix_0_values:
            #print("Changing indexes")
            #print(np.where(beta==0))
            #print(np.where(power == np.inf))
            power[beta == 0] = 1
        
        return power
    
    @staticmethod
    def remove_edges(ts, ps, edge):
        valid_indexes = np.abs(ts) <= edge
        ts = ts[valid_indexes]
        ps = ps[valid_indexes]
        return ts, ps 

        
    def _fit(self, edge=0.03):
        # It doesnt make much difference but it makes the errors much smaller
        if edge:
            ts, ps = self.remove_edges(self.norm_thetas, self.norm_power, edge=edge)
        
        popt, pcov = curve_fit(self._calc_power, ts, ps, bounds=self._fit_bounds)
                
        self.param_errs = np.sqrt(np.diag(pcov))
        self._popt = popt 
        
    @property
    def kb(self):
        return self._popt[0]

    @property
    def center(self):
        return self._popt[1]
        
    def print_fit_params(self):
        print_params(["kb", "center"], [self.kb, self.center], self.param_errs)
        
    def _plot_raw(self):
        fig, ax = plt.subplots()
        ax.set_title("Normalized Power vs Centered Theta")
        ax.set_xlabel("Theta [Rads]")
        ax.set_ylabel("Normalized Power")
        ax.plot(self.norm_thetas, self.norm_power, '.-', label="Measured data")
        
    def plot(self, slit_width=None):
        self._plot_raw()

        fig, ax = plt.subplots()
        ax.set_title("Normalized Power vs Centered Theta")
        ax.set_xlabel("Theta [Rads]")
        ax.set_ylabel("Normalized Power")
        ax.plot(self.norm_thetas, self.norm_power, '.', label="Measured data")
        
        ax.annotate(f"Slit width={slit_width}[m]", xy=(0.05, 0.95), xycoords='axes fraction')
        
        xx = np.arange(1.05*np.min(self.norm_thetas), 1.05*np.max(self.norm_thetas), 0.0001)
        yy = self._calc_power(xx, *self._popt)
        ax.plot(xx, yy, '--', label=f"Fit to {self._theoretical_power_shape}")
        ax.legend()
    
    def residuals(self):
        plt.figure("Residuals")
        
        yy = self._calc_power(self.norm_thetas, *self._popt)
        plt.plot(self.norm_thetas, self.norm_power)
        plt.plot(self.norm_thetas, yy)
        difference = yy - self.norm_power 
        plt.plot(self.norm_thetas, difference,'or')
        plt.grid()
        plt.title("Residuals")
        
class DoubleSlitInterferenceFit(InterferenceFit):
    _theoretical_power_shape = "sinc*cos^2"
    _fit_bounds = ((0, -0.001, 0), (1e4, 0.001, 1e7))

    def _calc_power(self, t, kb, c, ka):
        power = super()._calc_power(t, kb, c)
        power *= np.cos(ka/2 * np.sin(t))**2
        
        return power
        
    def print_fit_params(self):
        print_params(["kb", "center", "ka"], [self.kb, self.center, self.ka], self.param_errs)

    @property
    def ka(self):
        return self._popt[2]

class NSlitInterferenceFit(DoubleSlitInterferenceFit):
    def __init__(self, vtor, path, N):
        super().__init__(self, vtor, path)
        self.N = N 

    def _calc_power(self, t, kb, c, ka):
        power = InterferenceFit._calc_power(self, t, kb, c)

        alpha = ka/2 * np.sin(t)
        Nfactor = (np.sin(self.N * alpha) / np.sin(alpha)) ** 2       
        
        Nfactor[alpha == 0] = 1
        
        power *= Nfactor
        
        return power
